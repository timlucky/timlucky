var tobi = 0;
$(document).ready(function () {
	$("#tobi_result_panel").jqxPanel({width: 700, height: 550, sizeMode: "fixed", scrollBarSize: 20, autoUpdate: true, theme: selectedTheme});
    $("#tobi_button").jqxButton({width: '150', theme: selectedTheme});
    $("#tobi_button").on('click', function () {
        $("#tobi_result").html('please wait ...');
        tobi = Math.random() * 1000;
        execPHP('tobi.php' + '?wert=' + tobi, 'tobi_result');
    });
});

//# sourceURL=tab_tobi.js
