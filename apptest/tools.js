/**
* dump array etc as like as in php
*
* @param {type} arr
* @param {type} level
* @returns {String}
*/
function print_r(arr, level) {
	var dumped_text = "";
	if (!level)
		level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for (var j = 0; j < level + 1; j++)
		level_padding += "    ";

	if (typeof(arr) === 'object') { //Array/Hashes/Objects
		for (var item in arr) {
			var value = arr[item];

			if (typeof(value) === 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' {<br>";
				dumped_text += print_r(value, level + 1);
				dumped_text += " }<br>";
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"<br>";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>" + arr + "<===(" + typeof(arr) + ")";
	}
	return dumped_text;
}


/**
* call PHP via AJAX
*
* @param url
* @param div
*/
var execPHP = function (url, div) {
	$.getJSON(url, function (data) {
		var jsonPretty = JSON.stringify(data, null, 2);
		$("#"+div).html("<pre>" + jsonPretty + "</pre>");
	});
}

/**
* call PHP via AJAX and call function with returned data
*
* @param url
* @param func
*/
var execPHPf = function(url, func) {
	console.log("execPHPf: calling url=" + url);
	$.getJSON(url, function (data) {
		console.log("execPHPf: done: calling func=" + func.name);
		func(data);
	});
}

function getprogress(id, func) {
	console.log("getprogress: for id=" + id);
	$.getJSON('getprogress.php' + '?id=' + id, function(data) {
		console.log("getprogress: done: calling func=" + func.name);
		func(data.value);
	});
}

//# sourceURL=tools.js
