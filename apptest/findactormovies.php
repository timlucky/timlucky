<?php

/**
* main function
*
*/
function main($search_actor = 0) {
	$os = array();

	$count = 0;

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	$osr = array();
	$sort = array('cinema_release_date' => -1, '_id' => -1);	// order of entered in db
	$query = array(
		'persons' => array( '$elemMatch' => array('name' => $search_actor))
	);
	$projection = array('_id' => 0, 'title' => 1, 'cinema_release_date' => 1);
	$cursor = safe_session('collection')->find($query, $projection)->sort($sort);
	while ($cursor->hasNext() ) {
		$count++;
		$o = $cursor->getNext();
		$crd = 'cinema_release_date';
		$year = substr(safe_array($o, $crd, ''), 0, 4);
		if ($year != '') {
			$year = ' (' . $year . ')';
		}
		$osr[] = $o['title'] . $year;
	}

	$os[] = "found $count movies";

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os[] = "needs $time seconds";
	$after = memory_get_usage(true);
	$os[] = "memory used = " . (int)(($after - $before)/1024/1024 + 0.999) . " Mbytes";

	$oa = array('result' => true, 'info' => $os, 'values' => $osr);

	return $oa;
}


////////
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_movies'));

$search_actor = safe_request('actor', 'George Clooney');
$r = main($search_actor);
echo json_encode($r);
?>
