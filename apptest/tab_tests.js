var gettitles_source;
var gettitles_dataAdapter;

// show "please wait" in div
function please_wait_test_query_result() {
	$("#test_query_result").html('please wait ...');
}

// callback function to display data from ajax
function callback_test_query_result(data) {
	var jsonPretty = JSON.stringify(data, null, 2);
	$("#test_query_result").html("<pre>" + jsonPretty + "</pre>");
//	console.log('callback_test_query_result: ' + jsonPretty);
}

$(function() {
	// Create jqxPanel
	$("#test_query_result_panel").jqxPanel({width: 700, height: 500, sizeMode: "fixed", scrollBarSize: 20, autoUpdate: true, theme: selectedTheme});
});

var found_id = 0;
$(document).ready(function() {
	$("#find_movie_query_button").jqxButton({width: '150', theme: selectedTheme});
	$("#find_movie_query_button").on('click', function() {
		please_wait_test_query_result();
		//		execPHP('testquery.php' + '?_id=' + found_id, 'test_query_result');
		execPHPf('testquery.php' + '?_id=' + found_id, callback_test_query_result);
	});
	$("#find_movie_query_button").jqxTooltip({ content: '<b>Info:</b> static list with titles starting 2012-01-01<br>... search for Title "Gravity" if input above left empty', position: 'mouse', name: 'input_tooltip', theme: selectedTheme});
});

var found_actor = 0;
$(document).ready(function() {
	$("#find_actor_query_button").jqxButton({width: '150', theme: selectedTheme});
	$("#find_actor_query_button").on('click', function() {
		please_wait_test_query_result();
		execPHPf('findactormovies.php' + '?actor=' + found_actor, callback_test_query_result);
	});
	$("#find_actor_query_button").jqxTooltip({ content: '<b>Info:</b> search dynamically/on demand<br>in database for Actor and print list of movies', position: 'mouse', name: 'input_tooltip', theme: selectedTheme});
});

$(document).ready(function() {
	// Create input title field

	// prepare the data
	gettitles_source =
	{
		datatype: "json",
		datafields: [
			{name: '_id'},
			{name: 'title'}
		],
		url: "gettitles.php"
	};
	gettitles_dataAdapter = new $.jqx.dataAdapter(gettitles_source,
				{
					loadComplete: function(data) {
						console.log(data.data.length + " titles loaded");
//						$("#staticTitles").removeAttr('disabled');
						$("#staticTitles").css('visibility', 'visible').hide().fadeIn('slow');
					}
				});

	// Create a jqxInput
	$("#jqxInput_title").jqxInput({source: gettitles_dataAdapter, placeHolder: "Title:", displayMember: "title", valueMember: "_id", width: 200, height: 25, theme: selectedTheme});
	$("#jqxInput_title").on('select', function(event) {
		if (event.args) {
			var item = event.args.item;
			if (item) {
				var valueelement = $("<div></div>");
				found_id = item.value;
				valueelement.text("Value: " + found_id);
				var labelelement = $("<div></div>");
				labelelement.text("Label: " + item.label);
				$("#test_query_result").children().remove();
				$("#test_query_result").append(labelelement);
				$("#test_query_result").append(valueelement);
			}
		}
	});
	$("#jqxInput_title").jqxTooltip({ content: '<b>Try:</b> type part of a title to get list of matches<i>(eg. hobb)</i>', position: 'mouse', name: 'input_tooltip', theme: selectedTheme});
});

$(document).ready(function() {
	// create input actor field
	$("#input_actor").jqxInput({
		theme: selectedTheme,
		placeHolder: "Name of actor/actress",
		height: 25,
		width: 200,
		items: 25,
		minLength: 3,
		source: function(query, response) {
			var dataAdapter = new $.jqx.dataAdapter
			(
				{
					datatype: "json",
					datafields:
					[
						{name: 'name'}
					],
					url: "getactor.php",
					root: 'data',
					data:
					{
						maxRows: 25
					}
				},
				{
					autoBind: true,
					formatData: function(data) {
						data.searchStringPart = query;
						return data;
					},
					loadComplete: function(data) {
						if (data.data.length > 0) {
							response($.map(data.data, function(item) {
								return {
									label: item.name,	// + " (Actor)",
									value: item.name
								}
							}));
						}
					}
				}
			);
		}
	});

	$("#input_actor").on('select', function(event) {
		if (event.args) {
			var item = event.args.item;
			if (item) {
				var valueelement = $("<div></div>");
				found_actor = item.value;
				valueelement.text("Value: " + found_actor);
				var labelelement = $("<div></div>");
				labelelement.text("Label: " + item.label);
				$("#test_query_result").children().remove();
				$("#test_query_result").append(labelelement);
				$("#test_query_result").append(valueelement);
			}
		}
	});
	$("#input_actor").jqxTooltip({ content: '<b>Try:</b> type part of a name to get list of matches<br> dynamically searching on server database of about 340.000 names<br><i>(min 3 chars)</i>', position: 'mouse', name: 'input_tooltip', theme: selectedTheme});
});

//# sourceURL=tab_tests.js
