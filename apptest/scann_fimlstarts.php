<?php
	require_once('libs/classes/no_sql.php');
    require_once('libs/classes/html_parser/filmstarts_parser.php');

    //exec('phantomjs savepage.js "http://www.imdb.com/company/co0026840/?ref_=fn_co_co_2" "data/html_sources/imdb_warner/imdb_warner.html"', $o, $e);

    $source_file = ('data/html_sources/filmstarts_warner/filmstarts_warner.html');
    $output = '';
    $source_file = '';
    $filmstarts_parser_parser = new FILMstarts_parser($source_file);
    $filmstarts_parser_parser->store_filmstarts_data();

	$no_sql = new No_SQL();
	$cursor = $no_sql->no_sql_find();

    $result_array = array();
    foreach($cursor as $n_key) {

    	foreach($n_key as $key => $val) {
			array_push($result_array, $n_key);
		}
    }
   $result_array = json_encode($result_array);

    header("Content-type: application/json");
    echo '{"data":' . $result_array . '}';
    /*
	unlink($source_file);

	$os = array();
	$os[] = "Saving Source successfully!";
	$no_sql = new No_SQL();

	//echo '<pre>';
    $cursor = $no_sql->no_sql_find();
    $result_array = array();
    foreach($cursor as $n_key) {
               array_push($result_array, $n_key);
    }
    $result_array = json_encode($result_array);

    header("Content-type: application/json");
    echo '{"data":' . $result_array . '}';
*/
?>
