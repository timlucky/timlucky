<?php
require_once('tools.php');

//error_log(__FILE__ . " : " . print_r($_REQUEST, true));

$format = safe_request('format', '');	// set in xls only
if ($format == '') {
	$format = pathinfo(safe_request('fname', ''), PATHINFO_EXTENSION);	// get from filename
}

switch ($format) {
case 'xls':
	header('Content-Type: application/ms-excel');
	header('Content-Disposition: attachment; filename="' . $_REQUEST['filename'] . '.' . $_REQUEST['format'] . '"');
	header("Content-Length: " . strlen($_REQUEST['content']));
	echo $_REQUEST['content'];
	break;
case 'jpg':
case 'jpeg':
	header('Content-Type: image/jpeg');
	header('Content-Disposition: attachment; filename="' . $_REQUEST['fname'] . '"');
	header("Content-Length: " . strlen(base64_decode($_REQUEST['content'])));
	echo base64_decode($_REQUEST['content']);
	break;
case 'png':
	header('Content-Type: image/png');
	header('Content-Disposition: attachment; filename="' . $_REQUEST['fname'] . '"');
	header("Content-Length: " . strlen(base64_decode($_REQUEST['content'])));
	echo base64_decode($_REQUEST['content']);
	break;
default:
	header('Content-Type: applicationforce-download');
	header('Content-Disposition: attachment; filename="' . $_REQUEST['fname'] . '"');
	header("Content-Length: " . strlen(base64_decode($_REQUEST['content'])));
	echo base64_decode($_REQUEST['content']);
	break;
}

?>
