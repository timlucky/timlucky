<?php
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_progress'));

$id = (int)safe_request('id', -1);

list($result, $v) = getprogress($id);
echo json_encode(array('result' => $result, 'value' => $v));
?>
