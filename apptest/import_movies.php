<?php
/**
* here were importing all needed key/values into document (and possibly mapping then)
*
* @param mixed $doc
* @param mixed $film
* @param mixed $dynlists
*/
function film2doc(&$doc, &$film, &$dynlists) {
	set_int($doc, $film, 'id', '_id');	// film (mediabiz) id will be main document key ("_id")
	set_string($doc, $film, 'titel', 'title');
	set_string($doc, $film, 'originalTitel', 'original_title');
	set_int($doc, $film, 'laufzeit', 'runtime_minutes');
	set_string($doc, $film, 'kinostart', 'cinema_release_date');
	if (isset($doc['cinema_release_date'])) {	// additionally calculate unix time stamp
		$doc['cinema_release_date_ts'] = (int)strtotime($doc['cinema_release_date']);
	}
	set_string($doc, $film, 'kinstart_usa', 'cinema_release_date_usa');
	set_bool($doc, $film, 'in_postproduktion', 'true', 'in_postproduction');
	dynset_string($doc, $film, 'bildformat', $dynlists, '', 'imageformat');
	dynset_string($doc, $film, 'tonformat', $dynlists, '', 'soundformat');

	// genre kann 2 durch slash ("/") getrennte angaben haben (z.B. "Action/Drama")... die dann separieren und einzeln verlinken und in array
	// also in dynlists['genre'] dann nur Action und Drama aufnehmen und hier in doc['genre'] ein array anlegen mit [0 => 'Action', 1 => 'Drama']
	dynset_string($doc, $film, 'genre', $dynlists, '', false, false, false, '/', $_SESSION['map_values_list']['genre']);
	dynset_string($doc, $film, 'genre', $dynlists, '', 'genres', false, false, '/', $_SESSION['map_values_list']['genre'], ', ');

	dynset_string($doc, $film, 'kategorie', $dynlists, '', 'category', false, false, false, $_SESSION['map_values_list']['category']);
	dynset_string($doc, $film, 'verleih', $dynlists, '', 'studio');
	set_bool($doc, $film, 'threed', 'true', '3d');
	dynset_string($doc, $film, 'reihe', $dynlists, '', 'series');
	dynset_string($doc, $film, 'fsk', $dynlists, '', 'german_rating');	// not int! (may be 'o.A.', '16', 'k.J.' etc)
	set_int($doc, $film, 'einspielergebniss_usa', 'boxoffice_gross_usa');
	set_int($doc, $film, 'einspielergebniss_deutschland', 'boxoffice_gross_germany');

	$c = count($film->charts_usa->chart);
	if ($c > 0) {
		$doc['charts_usa'] = array();
		$d1 = &$doc['charts_usa'];
		$d1['chart'] = array();
		$d2 = &$d1['chart'];
		$d1['copies_overall'] = 0;
		$d1['dollar_copies_overall'] = 0;
		$d1['boxoffice_gross_we_overall'] = 0;
		$d1['boxoffice_gross_overall'] = 0;
		for ($i = 0; $i < $c; $i++) {
			set_int($d2[$i], $film->charts_usa->chart[$i], 'woche', 'week');
			set_string($d2[$i], $film->charts_usa->chart[$i], 'datum_von', 'date_from');
			set_string($d2[$i], $film->charts_usa->chart[$i], 'datum_bis', 'date_to');
			set_int($d2[$i], $film->charts_usa->chart[$i], 'platz', 'rank');
			set_int($d2[$i], $film->charts_usa->chart[$i], 'kopien', 'copies');
			set_int($d2[$i], $film->charts_usa->chart[$i], 'einspielergebnis_we', 'boxoffice_gross_we');
			set_int($d2[$i], $film->charts_usa->chart[$i], 'dollar_pro_kopie', 'dollar_per_copy');
			set_int($d2[$i], $film->charts_usa->chart[$i], 'einspielergebnis_gesamt', 'boxoffice_gross_overall');
			$d1['copies_overall'] += $d2[$i]['copies'];
			$d1['boxoffice_gross_we_overall'] += $d2[$i]['boxoffice_gross_we'];
			$d1['dollar_copies_overall'] += ($d2[$i]['copies'] * $d2[$i]['dollar_per_copy']);
			$d1['boxoffice_gross_overall'] = $d2[$i]['boxoffice_gross_overall'];
		}
	}

	$c = count($film->charts_deutschland->chart);	// ATTENTION: may be: empty (0) or only one (1) or array of "chart" (>1) - generate array ALWAYS if any is available
	if ($c > 0) {
		$doc['charts_germany'] = array();
		$d1 = &$doc['charts_germany'];
		$d1['chart'] = array();
		$d2 = &$d1['chart'];
		$d1['copies_overall'] = 0;
		$d1['euro_we_overall'] = 0;
		$d1['visitors_we_overall'] = 0;
		$d1['visitors_overall'] = 0;
		for ($i = 0; $i < $c; $i++) {
			set_int($d2[$i], $film->charts_deutschland->chart[$i], 'woche', 'week');
			set_string($d2[$i], $film->charts_deutschland->chart[$i], 'datum_von', 'date_from');
			set_string($d2[$i], $film->charts_deutschland->chart[$i], 'datum_bis', 'date_to');
			set_int($d2[$i], $film->charts_deutschland->chart[$i], 'platz', 'rank');
			set_int($d2[$i], $film->charts_deutschland->chart[$i], 'kopien', 'copies');
			set_int($d2[$i], $film->charts_deutschland->chart[$i], 'euro');
			set_int($d2[$i], $film->charts_deutschland->chart[$i], 'besucher_we', 'visitors_we');
			set_int($d2[$i], $film->charts_deutschland->chart[$i], 'besucher_gesamt', 'visitors_overall');
			$d1['copies_overall'] += $d2[$i]['copies'];
			$d1['euro_we_overall'] += $d2[$i]['euro'];
			$d1['visitors_we_overall'] += $d2[$i]['visitors_we'];
			$d1['visitors_overall'] = MAX($d1['visitors_overall'], $doc['charts_germany']['chart'][$i]['visitors_overall']);	// last entry may NOT be maximum (see {_id: 45517}
		}
	}

	// omit entries:
	// renttrack_alt
	// renttracks

	$c = count($film->dvd_fassungen->dvd);
	if ($c > 0) {
		$doc['media'] = array();
		$d1 = &$doc['media'];
		$d1['dvd'] = array();
		//		$d2 = &$d1['dvd'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			set_int($d2[$i], $film->dvd_fassungen->dvd[$i], 'id');
			set_string($d2[$i], $film->dvd_fassungen->dvd[$i], 'titel', 'title');
			set_bool($d2[$i], $film->dvd_fassungen->dvd[$i], 'kaufmedium', 'J', 'buymedia');	// hier aus "J", "N" ein boolean machen
			set_bool($d2[$i], $film->dvd_fassungen->dvd[$i], 'leihmedium', 'J', 'lendmedia');	// hier aus "J", "N" ein boolean machen
			set_string($d2[$i], $film->dvd_fassungen->dvd[$i], 'system');	// TODO: hier ggf. eine dynlist anlegen ?
			set_string($d2[$i], $film->dvd_fassungen->dvd[$i], 'release_date');
			set_string($d2[$i], $film->dvd_fassungen->dvd[$i], 'ean');
		}
	}

	$c = count($film->cast_crew->person);
	if ($c > 0) {
		$doc['persons'] = array();
		$d1 = &$doc['persons'];
		$d2 = &$d1;
		$director = '';
		$director_count = 0;
		$top4actors = '';
		$top4actors_count = 0;
		for ($i = 0; $i < $c; $i++) {
			//DONE: omit id ... not used ?! (may be to distinguish two persons with same name ...)
			//			set_int($d2[$i], $film->cast_crew->person[$i], 'id');
			//DONE: really needed person as dynlists ??? (will be 390000 persons for 82000 movies) ... and searching in mongo for persons is really fast (and 390000 are too big to have them as filter ("search as you type) ...
			//			dynset_string($d2[$i], $film->cast_crew->person[$i], 'voller_name', $dynlists, '', 'person', false, $d2[$i]['id']);	// BTW: each individual person has its own id - regardless of his actual function
			set_string($d2[$i], $film->cast_crew->person[$i], 'voller_name', 'name');	// not using dynlist for persons !
			dynset_string($d2[$i], $film->cast_crew->person[$i], 'rolle_funktion', $dynlists, '', 'function', false, false, false, $_SESSION['map_values_list']['function']);
			//TODO: possibly fetch needed infos here (aggregate): list of directors ("Regie") and list of top-4-actors (all as simple comma-separated string only for faster display)
			//DONE: omit "persons" array : 0...n directly beyond cast_crew
			//DONE: additionally map "value" ("Regie", "Darsteller" to corresponding english values): dynset_string(...., $map_values_list) mit $map_values_list["Regie" => "Director", "Darsteller" => "Actor"] ...
			if ($director_count < 1) {
				if ($d2[$i]['function'] == 'Director') {
					$director = $d2[$i]['name'];
					$director_count++;
				}
			}
			if ($top4actors_count < 4) {
				if ($d2[$i]['function'] == 'Actor') {
					$top4actors .= ($top4actors_count ? ', ' : '') . $d2[$i]['name'];
					$top4actors_count++;
				}
			}

		}
		if ($director_count > 0) {
			//			$d2['director'] = $director;	// will prevent searching in numbered array ...
			$doc['director'] = $director;
		}
		if ($top4actors_count > 0) {
			//			$d2['top4actors'] = $top4actors;
			$doc['top4actors'] = $top4actors;
		}
	}

	$c = count($film->produktionsfirmen->firma);
	if ($c > 0) {
		$doc['production_companies'] = array();
		$d1 = &$doc['production_companies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//DONE: we dont really need a list of all companys !?!
			//			dynset_string($d2[$i], $film->produktionsfirmen->firma[$i], 'name', $dynlists, 'company_');
			set_string($d2[$i], $film->produktionsfirmen->firma[$i], 'name');
		}
	}
	$c = count($film->koproduktionsfirmen->firma);
	if ($c > 0) {
		$doc['coproduction_companies'] = array();
		$d1 = &$doc['coproduction_companies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->koproduktionsfirmen->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all companys !?!
			set_string($d2[$i], $film->koproduktionsfirmen->firma[$i], 'name');
		}
	}
/* NO subsidies for now ... its overhead until now
	$c = count($film->senderbeteiligung->firma);
	if ($c > 0) {
		$doc['broadcastcontribution'] = array();
		$d1 = &$doc['broadcastcontribution'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->senderbeteiligung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all companys !?!
			set_string($d2[$i], $film->senderbeteiligung->firma[$i], 'name');
		}
	}
	$c = count($film->foerderung->firma);
	if ($c > 0) {
		$doc['subsidies'] = array();
		$d1 = &$doc['subsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->foerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->foerderung->firma[$i], 'name');
			set_int($d2[$i], $film->foerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->foerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->projektfoerderung->firma);
	if ($c > 0) {
		$doc['projectsubsidies'] = array();
		$d1 = &$doc['projectsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->projektfoerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->projektfoerderung->firma[$i], 'name');
			set_int($d2[$i], $film->projektfoerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->projektfoerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->produktionsfoerderung->firma);
	if ($c > 0) {
		$doc['productionsubsidies'] = array();
		$d1 = &$doc['productionsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->produktionsfoerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->produktionsfoerderung->firma[$i], 'name');
			set_int($d2[$i], $film->produktionsfoerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->produktionsfoerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->verleihfoerderung->firma);
	if ($c > 0) {
		$doc['rentalsubsidies'] = array();
		$d1 = &$doc['rentalsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->verleihfoerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->verleihfoerderung->firma[$i], 'name');
			set_int($d2[$i], $film->verleihfoerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->verleihfoerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->drehbuchfoerderung->firma);
	if ($c > 0) {
		$doc['scriptsubsidies'] = array();
		$d1 = &$doc['scriptsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->drehbuchfoerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->drehbuchfoerderung->firma[$i], 'name');
			set_int($d2[$i], $film->drehbuchfoerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->drehbuchfoerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->vorbereitungsfoerderung->firma);
	if ($c > 0) {
		$doc['preparationsubsidies'] = array();
		$d1 = &$doc['preparationsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->vorbereitungsfoerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->vorbereitungsfoerderung->firma[$i], 'name');
			set_int($d2[$i], $film->vorbereitungsfoerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->vorbereitungsfoerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->stoffentwicklungsfoerderung->firma);
	if ($c > 0) {
		$doc['storydevelopmentsubsidies'] = array();
		$d1 = &$doc['storydevelopmentsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->stoffentwicklungsfoerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->stoffentwicklungsfoerderung->firma[$i], 'name');
			set_int($d2[$i], $film->stoffentwicklungsfoerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->stoffentwicklungsfoerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->medialeistunden->firma);
	if ($c > 0) {
		$doc['mediasubsidies'] = array();
		$d1 = &$doc['mediasubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->medialeistunden->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->medialeistunden->firma[$i], 'name');
			set_int($d2[$i], $film->medialeistunden->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->medialeistunden->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
	$c = count($film->postprod_foerderung->firma);
	if ($c > 0) {
		$doc['postproductionsubsidies'] = array();
		$d1 = &$doc['postproductionsubsidies'];
		$d2 = &$d1;
		for ($i = 0; $i < $c; $i++) {
			//			dynset_string($d2[$i], $film->postprod_foerderung->firma[$i], 'name', $dynlists, 'company_');	//DONE: we dont really need a list of all
			set_string($d2[$i], $film->postprod_foerderung->firma[$i], 'name');
			set_int($d2[$i], $film->postprod_foerderung->firma[$i], 'foerderungshoehe', 'value');
			dynset_string($d2[$i], $film->postprod_foerderung->firma[$i], 'waehrung', $dynlists, '', 'currency');	// dynlists ?
		}
	}
*/

	set_string($doc, $film, 'trailerMp4');
	set_string($doc, $film, 'trailerFlv');
	set_string($doc, $film, 'posterUrl');

	//DONE: omit because e-media.de links (which is mediabiz of course)
	//	$c = count($film->bilder->bildUrl);
	//	if ($c > 0) {
	//		$doc['images'] = (array)$film->bilder->bildUrl;
	//	}

	set_string($doc, $film, 'inhalt', 'topic');
	set_string($doc, $film, 'kritik', 'review');
	set_string($doc, $film, 'kurztext', 'synopsisshort');
	set_string($doc, $film, 'langbesprechung', 'synopsislong');
	set_string($doc, $film, 'kinoLink', 'cinemalink');
	//	set_string($doc, $film, 'mediabizLink', 'mediabizlink');	// omit !?

	//DONE: possibly put whole xml into document ... (as string) :) ... but not for our texts ...
	//	$doc['xml'] = $film;
}



/**
* callback function (reads xml and write to db)
*
*/
function import_movies($update = true) {
	$fnam = __FUNCTION__;
	$each = safe_session($fnam . '_param_each');

	// xml error handler
	libxml_use_internal_errors(true);

	//load xml
	$xml = simplexml_load_string(safe_session($fnam . '_param_xml'));

	// xml error handler
	if (!$xml) {
		$xmll = explode("\n", safe_session($fnam . '_param_xml'));
		$errors = libxml_get_errors();
		$es = '';
		foreach ($errors as $error) {
			$es .= display_xml_error($error, $xmll);
		}
		libxml_clear_errors();
		die(nl2br($es));
	}

	// get current dynlists
	$dynlists = &safe_session_r('dynlists', array());

	$inserted = 0;
	$updated = 0;
	foreach ($xml->$each as $film) {
		// init empty doc
		$doc = array();

		// call reader
		film2doc($doc, $film, $dynlists);

		//error_log("document=" . print_r($doc, true));
		if (true) {
			$query = array( '_id' => $doc['_id'] );
			if ($update) {
				$r = safe_session('collection')->update($query, $doc, array( 'upsert' => true, 'multi' => false ));
				if ($r['updatedExisting'] == false) {
					$inserted++;
				} else {
					$updated++;
				}
			} else {
				try {
					$r = safe_session('collection')->insert($doc);
					$inserted++;
				} catch(MongoCursorException $e) {
					// twice entered ... aborted ... updating
					$r = safe_session('collection')->update($query, $doc, array( 'upsert' => true, 'multi' => false ));
					if ($r['updatedExisting'] == false) {
						$inserted++;
					} else {
						$updated++;
					}
				}
			}
			//o($doc);
		}
	}

	return array('inserted' => $inserted, 'updated' => $updated);
}


/**
* handle xml function to read xml in parts and call callback
*
* @param mixed $callback
* @param mixed $file
* @param mixed $root
* @param mixed $each
* @param mixed $partsize
*
* @return array(inserted, updated)
*/
function handle_xml($callback, $file, $root, $each, $partsize = 1048576, $callback_params = array(), $progress_id = 1) {
	$root_s = '<' . $root . '>';
	$root_sl = strlen($root_s);
	$root_e = '</' . $root . '>';
	$each_s = '</' . $each . '>';
	$each_sl = strlen($each_s);

	$_SESSION[$callback . '_param_each'] = $each;

	// handle chunks
	if (!file_exists($file)) {
		return -1;
	}
	$rest = '';
	$header = '';

	$firstrun = true;
	$run = 0;
	$infos = array('inserted' => 0, 'updated' => 0);

	// set 0% to start js timer
	$filelength = filesize($file);	// for progress indication
	$time_start = microtime(true);	// for progress indication
	setprogress($progress_id, -1, $infos);

	// open file and read chunks of given size
	$fp = fopen($file, 'rb');
	while ($read = fread($fp, $partsize)) {
		$run++;
		//		echo "run=$run<br>"; ob_flush(); flush();

		// fetch xml header and root (only on 1st run)
		$rp = 0;
		if ($firstrun) {
			$rp = strpos($read, $root_s, 0);
			$rp += ($rp !== false) ? $root_sl : 0;
			$header = substr($read, 0, $rp);
			$firstrun = false;
		}

		// position of last $each in read chunk
		$last_each = strrpos($read, $each_s);
		if ($last_each === false) {	// none found ... fetch another chunk and append
			$rest .= $read;
			continue;
		}
		$last_each += ($last_each !== false) ? $each_sl : 0;

		// create valid xml
		$_SESSION[$callback . '_param_xml'] = $header . $rest . substr($read, $rp, $last_each - $rp) . $root_e;

		// call callback (to import xml)
		$r = call_user_func_array($callback, $callback_params);
		$infos['inserted'] += $r['inserted'];
		$infos['updated'] += $r['updated'];

		$rest = substr($read, $last_each);	// ending chunk bytes as leading for next chunk

		// progress indication
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		if ($time >= 5) {	// each 5 second push update
			$percent = max(0, min(100, (int)(($partsize * $run) / $filelength * 100)));
			setprogress($progress_id, $percent, $infos);
			$time_start = $time_end;
		}
	}
	fclose($fp);

	setprogress($progress_id, 100, $infos);	// set 100% to stop js timer

	// release memory used
	$rest = '';
	$_SESSION[$callback . '_param_xml'] = '';

	return $infos;
}


function global_inits() {
	// global map list
	$_SESSION['map_values_list'] = array();
	$_SESSION['map_values_list']['function'] = array(
		'Regie' => 'Director'
		, 'Buch' => 'Story'
		, 'Darsteller' => 'Actor'
		, 'Kamera' => 'Camera'
		, 'Musik' => 'Soundtrack'
		, 'Produktion' => 'Production'
		, 'Buchvorl.' => 'BasedStory'
		, 'Schnitt' => 'Cut'
		, 'Ausstattg.' => 'Equipment'
		, 'Casting' => 'Casting'
		, 'Ausf. Pro.' => 'ExecutiveProducer'
		, 'Koprodukt.' => 'Coproduction'
		, 'Kostüme' => 'Costumes'
		, 'Maske' => 'Makeup'
		, 'Effekte' => 'Effects'
		, 'Ton' => 'Sound'
		, 'Prod.-ltg.' => 'ProductionAdministration'
		, 'Sprecher' => 'VoiceOverArtist'
		, 'Choreograf' => 'Choreographer'
		, 'Animation' => 'Animation'
		, 'Hers.-ltg.' => 'ManufacturingAdministration'
		, 'Producer' => 'Producer'
		, 'SyncStimme' => 'Dubbing'
		, 'Idee' => 'Concept'
		, 'Dirigent' => 'Conductor'
		, 'Redaktion' => 'Editor'
		, 'Ausf. Kop.' => 'ExecutiveCoproducer'
		, 'Line Prod.' => 'LineProduction'
		, 'Postprod.' => 'Postproduction'
	);
	$_SESSION['map_values_list']['category'] = array(
		'Spielfilm' => 'Movie'
		, 'Dokumentarfilm' => 'Documentary'
		, 'Spielfilm (TV)' => 'MovieTV'
		, 'Paket Spielf.' => 'BundleFeature'
	);
	$_SESSION['map_values_list']['genre'] = array(
		'Komödie' => 'Comedy'
		, 'Thriller' => 'Thriller'
		, 'Erotik' => 'Erotic'
		, 'Abenteuer' => 'Adventure'
		, 'Kriminalfilm' => 'Crime'
		, 'Drama' => 'Drama'
		, 'Melodram' => 'Melodrama'
		, 'Kinderfilm' => 'Kids'
		, 'Western' => 'Western'
		, 'Musikfilm' => 'MusicalMovie'
		, 'Musical' => 'Musical'
		, 'Action' => 'Action'
		, 'Science Fiction' => 'ScienceFiction'
		, 'Horror' => 'Horror'
		, 'Eastern' => 'Eastern'
		, 'Dokumentarfilm' => 'Documentary'
		, 'Film' => 'Movie'
		, 'Kriegsfilm' => 'WarMovie'
		, 'Zeichentrick' => 'Animatic'
		, 'Historienfilm' => 'Periodic'
		, 'Satire' => 'Satire'
		, 'Fantasy' => 'Fantasy'
		, 'Monumentalfilm' => 'Epic'
		, 'Kunst' => 'Art'
		, 'Heimatfilm' => 'Regional'
		, 'Sport' => 'Sport'
		, 'Musik' => 'Music'
		, 'Lovestory' => 'Lovestory'
		, 'Gesellschaft' => 'Society'
		, 'Zeitgeschichte' => 'Contemporary'
		, 'Geschichte' => 'Chronicle'
		, 'Episodenfilm' => 'Anthology'
		, 'Märchen (Realfilm)' => 'FairyTaleReal'
		, 'Experimentalfilm' => 'Experimental'
		, 'Puppenfilm' => 'PuppetMovie'
		, 'Porträt' => 'Portrait'
		, 'Biographie' => 'Biography'
		, 'Politik' => 'Politics'
		, 'Geographie' => 'Geography'
		, 'Wissenschaft' => 'Science'
		, 'Werbung' => 'Advertising'
		, 'Religion' => 'Religion'
		, 'Doku-Drama' => 'DocumentaryDrama'
		, 'Tiere' => 'Animals'
		, 'Trickfilm' => 'Animation'
		, 'Familie' => 'Family'
		, 'Biologie' => 'Biology'
		, 'Natur' => 'Nature'
		, 'Literatur' => 'Literature'
		, 'Tanz' => 'Dance'
		, 'Psychologie' => 'Psychology'
		, 'Militär' => 'Military'
		, 'Rock' => 'Rock'
		, 'Medizin' => 'Medical'
		, 'Technik' => 'Engineering'
		, 'EDV' => 'Computer'
		, 'Ethnologie' => 'Ethnology'
		, 'Reggae' => 'Reggae'
		, 'Jugend' => 'Youth'
		, 'Tragikomödie' => 'Tragicomedy'
		, 'Raumfahrt' => 'Space'
		, 'Mystery' => 'Mystery'
		, 'Auto' => 'Automobile'
		, 'Ökologie' => 'Ecology'
		, 'Wirtschaft' => 'Economy'
		, 'Jazz' => 'Jazz'
		, 'Astronomie' => 'Astronomy'
		, 'Heavy Metal' => 'HeavyMetal'
		, 'Kochen' => 'Cooking'
		, 'Backen' => 'Bake'
		, 'Hobby' => 'Hobby'
		, 'Bollywood' => 'Bollywood'
		, 'Tanzen' => 'Dance'
		, 'Independent' => 'Independent'
		, 'Architektur' => 'Architecture'
		, 'Luftfahrt' => 'Aviation'
		, 'Kultur' => 'Cultural'
		, 'Kostümfilm' => 'CostumeDrama'
		, 'Pop' => 'Pop'
	);
}


/**
* main function
*
* @param mixed $partialUpdate
*/
function main($partialUpdate = true, $progress_id = 1) {
	$os = '';

	if ($partialUpdate) {
		$file = './data/movies_2014_01_03.xml';	// update (daily)
		$files = glob('./data/movies_????_??_??.xml');
		$update = true;
	} else {
		$file = './data/movies_2013_08_28_full.xml';	// full base
		//		$file = 'movies_2013_12_19.xml';	// update (daily)
		safe_session('db')->drop();	// drop db (not only collection here) ... YET!
		$update = false;
	}
	$os .= "<br>using file $file";
	$root = 'filme';
	$each = 'film';
	$partsize = 1024*1024*4;	// 4M parts to fetch from input file
	$callback = 'import_movies';

	global_inits();

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	// init and read dynlists (possibly drop before also)
	$_SESSION['dynlists'] = array();
	read_dynlists($_SESSION['dynlists']);

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os .= "<br>after reading dynlists in $time seconds";
	$time_start = $time_end;
	$after = memory_get_usage(true);
	$os .= " - memory used=($after - $before)=" . ($after - $before);
	$before = $after;

	// read xml
	$r = handle_xml($callback, $file, $root, $each, $partsize, array($update), $progress_id);
	$os .= "<br>inserted " . $r['inserted'] . " + updated " . $r['updated'] . " movies";

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os .= "<br>after importing xml in $time seconds";
	$time_start = $time_end;
	$after = memory_get_usage(true);
	$os .= " - memory used=($after - $before)=" . ($after - $before);
	$before = $after;

	// write back dynlists
	write_dynlists($_SESSION['dynlists']);

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os .= "<br>after writing dynlists in $time seconds";
	$after = memory_get_usage(true);
	$os .= " - memory used=($after - $before)=" . ($after - $before);

	$oa = array('result' => true, 'values' => $os);

	return $oa;
}

////////
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_movies'));

$progress_id = 1;	// should be in config

$partialUpdate = ((int)safe_request('use_xml_update', true) != 0) ? true : false;

list($result, $v) = getprogress($progress_id);
if ($result) {
	if ($v != 0 && $v != 100) {	// seems to work ... abort
		die(json_encode(array('result' => false, 'info' => 'seems still importing ...')));
	}
}

$r = main($partialUpdate, $progress_id);
echo json_encode($r);
?>
