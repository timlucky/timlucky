
var tobi = 0;

$(document).ready(function () {
    $("#scann_filmstarts_button").jqxButton({ width: '120', theme: selectedTheme});

    $("#scann_filmstarts_button").on('click', function () {
        $("#scanner_filmstarts_result").html('please wait ...');
        tobi = Math.random() * 1000;
        var execute = execPHP('scann_fimlstarts.php' + '?wert=' + tobi, 'scanner_filmstarts_result', 'Saved "http://www.imdb.com/company/co0026840/?ref_=fn_co_co_2" successfully!');
        //filmstarts_grid();
        //$("#scanner_filmstarts_result").html('');
    });
});

function filmstarts_grid(){
				// prepare the data
			var showall_source = {
				datatype: "json",
				datafields: [
					{name: '_id', type: 'number'},
					{name: 'title', type: 'string'},
					{name: 'href', type: 'string'},
					{name: 'release_date', type: 'date'}
					//{name: 'visitors_overall', map: 'charts_germany>visitors_overall', type: 'number'},
					//{name: '3d', type: 'bool'}
				],
				id: 'id',
				url: "scanner.php",
				processdata: function (data) {
					//						data.maxRows = 50;
					console.log("process data");
				},
				pager: function (pagenum, pagesize, oldpagenum) {
					// callback called when a page or page size is changed.
					console.log("pager: pagenum=" + pagenum + " pagesize=" + pagesize + " oldpagenum=" + oldpagenum);
				},
				//					beforeprocessing: function (data) {
				//						showall_source.totalrecords = data.TotalRows;
				//					},
				//					sort: function () {
				// update the grid and send a request to the server.
				//						$("#jqxgrid").jqxGrid('updatebounddata');
				//					},
				root: 'data'
			};

			var showall_dataAdapter;
			showall_dataAdapter = new $.jqx.dataAdapter(showall_source, {
				downloadComplete: function (data, status, xhr) {
					//								console.log(print_r(data));
					//								console.log(print_r(data.count));
					$('#count').html("" + data['allcount'] + " entries in database ... showing " + data['count'] + " of them (" + (data.data.length) + ")");
				},
				loadComplete: function (data) {
					//					$("#jqxgrid").jqxGrid('autoresizecolumns');
				},
				loadError: function (xhr, status, error) {
					console.log("loadError");
				}
			});

			var columnrenderer = function (value) {
				return '<div style="text-align: center; font-style:italic;">' + value + '</div>';
			}

			$("#imdb_jqxgrid").jqxGrid({
				theme: selectedTheme,
				width: 1100,
				//				width: '90%',
				height: '70%',
				columnsheight: 50,
				source: showall_dataAdapter,
				showfilterrow: true,
				filterable: true,
				sortable: true,
				//				pageable: true,
				//				autoheight: true,
				autoshowfiltericon: true,
				columnsresize: true,
				columnsreorder: true,
				//							virtualmode: true,
				//							rendergridrows: function () {
				//								return showall_dataAdapter.records;
				//							},
				selectionmode: 'singlerow',
				showaggregates: true,
				showstatusbar: true,
				statusbarheight: 50,
				columns: [
					//					{ text: 'ID', datafield: '_id', cellsalign: 'right', width: 75, renderer: columnrenderer },
					{text: 'Title (fixed only for demonstration)', datafield: 'title', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 450, pinned: true},
					{text: 'Cinema Release', datafield: 'release_date', filtertype: 'date', width: 230, cellsalign: 'right', cellsformat: 'dddd d MMM yyyy-MM-dd'},
					{text: 'URL', datafield: 'href', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 450}
					/*
					{text: '3D', datafield: '3d', columntype: 'checkbox', filtertype: 'bool', width: 67
						,aggregates: [{ '3D':
							function (aggregatedValue, currentValue) {
								if (currentValue) {
									return aggregatedValue + 1;
								}
								return aggregatedValue;
							}
							},
							{ '2D':
								function (aggregatedValue, currentValue) {
									if (!currentValue) {
										return aggregatedValue + 1;
									}
									return aggregatedValue;
								}
							}
						]
					}*/
				]
			});
			$('#clearfilteringbutton').jqxButton({ height: 25, theme: selectedTheme});
			$('#clearfilteringbutton').click(function () {
				$("#imdb_jqxgrid").jqxGrid('clearfilters');
			});

			$("#excelExport").jqxButton({ theme: selectedTheme });
			$("#excelExport").click(function () {
                $("#imdb_jqxgrid").jqxGrid('exportdata', 'xls', 'jqxGrid', true, null, true, 'download.php');
            });

			$('#events').jqxPanel({ width: 300, height: 300, theme: selectedTheme});

			$("#imdb_jqxgrid").jqxGrid('selectionmode', 'singlerow');
			var selected_row = null;
			$("#imdb_jqxgrid").on('rowselect', function (event) {
				selected_row = event.args.rowindex;
				var data = $('#imdb_jqxgrid').jqxGrid('getrowdata', selected_row);
				var href = data.href;
				window.open('http://www.imdb.com/' + href,'_blank');
				/*
				$('#chartWindow').jqxWindow('open');
				$('#chartWindow').jqxWindow({title: data.title});
				chart_id = data._id;
				//				console.log('');
				//				console.log('---------------------');
				//				console.log('get chart for id=' + chart_id);
				chart_source.data = {_id: chart_id};
				//				console.log("1:dataBind():");
				chart_dataAdapter.dataBind();
				//				console.log("2:ok(databind)");
				if (chart_dataAdapter.records.length > 0) {
					//					console.log("setup chart for id=" + chart_id);
					//					console.log("update:");
					$('#jqxChart').jqxChart('update');
					//					console.log("set...");
					chart_settings.categoryAxis.tickMarksInterval = Math.max(1, Math.round(chart_data_count / 12));
					chart_settings.categoryAxis.unitInterval = Math.max(1, Math.round(chart_data_count / 6));
					chart_settings.categoryAxis.gridLinesInterval = Math.max(1,  Math.round(chart_data_count / 6));
					chart_settings.seriesGroups[0].valueAxis.unitInterval = Math.round(chart_data_visitors_overall_max / 50000) * 50000 / 10;
					chart_settings.seriesGroups[0].valueAxis.maxValue = chart_data_visitors_overall_max * 1.01;

					//					console.log("jqxChart.refresh:");
					$('#jqxChart').jqxChart({showToolTips: true});	// ... because tooltips wont work within jqxwindow (because of wrong z-index ...) [V 3.1.0]
					$('#jqxChart').jqxChart('refresh');
					//					console.log("3:done");
				} else {					// keine daten ...
					console.log("no data for id=" + chart_id);
				}
				*/
			});

			$("#imdb_jqxgrid").jqxTooltip({ content: '<b>Try:</b> click on row to open<br>current IMBD Website <br> in new window', position: 'mouse', name: 'input_tooltip', theme: selectedTheme});

			$("#imdb_jqxgrid").on("pagechanged", function (event) {
				//							console.log("pagechanged");
				//				$("#jqxgrid").jqxGrid('autoresizecolumns');
				//							$("#eventslog").css('display', 'block');
				//							if ($("#events").find('.logged').length >= 5) {
				//								$("#events").jqxPanel('clearcontent');
				//							}
				//							var args = event.args;
				//							var eventData = "pagechanged <div>Page:" + args.pagenum + ", Page Size: " + args.pagesize + "</div>";
				//							$('#events').jqxPanel('prepend', '<div class="logged" style="margin-top: 5px;">' + eventData + '</div>');
				//							// get page information.
				//							var paginginformation = $("#jqxgrid").jqxGrid('getpaginginformation');
				//							$('#paginginfo').html("<div style='margin-top: 5px;'>Page:" + paginginformation.pagenum + ", Page Size: " + paginginformation.pagesize + ", Pages Count: " + paginginformation.pagescount + "</div>");
			});
			$("#imdb_jqxgrid").on("pagesizechanged", function (event) {
				//							console.log("pagesizechanged");
				//				$("#jqxgrid").jqxGrid('autoresizecolumns');
				//							$("#eventslog").css('display', 'block');
				//							$("#events").jqxPanel('clearcontent');
				//							var args = event.args;
				//							var eventData = "pagesizechanged <div>Page:" + args.pagenum + ", Page Size: " + args.pagesize + ", Old Page Size: " + args.oldpagesize + "</div>";
				//							$('#events').jqxPanel('prepend', '<div style="margin-top: 5px;">' + eventData + '</div>');
				//							// get page information.
				//							var paginginformation = $("#jqxgrid").jqxGrid('getpaginginformation');
				//							$('#paginginfo').html("<div style='margin-top: 5px;'>Page:" + paginginformation.pagenum + ", Page Size: " + paginginformation.pagesize + ", Pages Count: " + paginginformation.pagescount + "</div>");
			});
			return false;
}


//# sourceURL=tab_scanneri.js