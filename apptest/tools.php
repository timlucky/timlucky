<?php

/**
* get config key
*
* @param mixed $k
* @param mixed $default
*/
function &_cfg($k, $default = false) {
	if (!isset($_SESSION['_cfg'][$k])) {
		$_SESSION[$k] = $default;
	}
	return $_SESSION['_cfg'][$k];
}


/**
* safe get SESSION variable (or default)
*
* @param mixed $k
* @param mixed $default
*/
function safe_session($k, $default = false) {
	if (isset($_SESSION[$k])) {
		return $_SESSION[$k];
	} else {
		return $default;
	}
}

/**
* safe get SESSION variable reference (and set default if not already exists)
*
* @param mixed $k
* @param mixed $default
*/
function &safe_session_r($k, $default = false) {
	if (!isset($_SESSION[$k])) {
		$_SESSION[$k] = $default;
	}
	return $_SESSION[$k];
}


/**
* safe get REQUEST variable (or default)
*
* @param mixed $k
* @param mixed $default
*/
function safe_request($k, $default = false) {
	if (isset($_REQUEST[$k])) {
		return $_REQUEST[$k];
	} else {
		return $default;
	}
}


/**
* safe get value from array with given key
*
* @param array $a
* @param mixed $k
* @param mixed $default
*/
function safe_array(&$a, &$k, $default = false) {
	if (isset($a[$k])) {
		return $a[$k];
	} else {
		return $default;
	}
}

/**
* output of html header
*
*/
function write_html_header() {
	$html_start = '<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
	<head>
	<meta charset="utf-8">
	<title>XML Test</title>
	</head>
	<body>
	';

	echo $html_start;
}


/**
* output of html footer
*
*/
function write_html_footer() {
	$html_end = '	</body>
	</html>';

	echo $html_end;
}



/**
* open and select a database
*
* @param mixed $dbname
* @param mixed $collectionname
*/
function open_database($dbname = '', $collection = '', $username = '', $password = '') {
	//	$username = '';
	//	$password = '';

	// connect
	$_SESSION['mongo'] = new MongoClient("mongodb://localhost");
	//$m = new MongoClient("mongodb://localhost", array("db" => $dbname, "username" => $username, "password" => $password));

	//TODO: handle errors

	$_SESSION['db_name'] = '';
	$_SESSION['collection_name'] = '';

	// select a database
	if ($dbname != '') {
		select_database($dbname);

		// select a collection (analogous to a relational database's table)
		if ($collection != '') {
			select_collection($collection);
		}
	}
	//TODO: handle errors

	return true;
}


/**
* select database
* return name of database
* if empty then return name of database only
*
* @param mixed $dbname
*/
function select_database($dbname = '') {
	if ($dbname != '') {
		$_SESSION['db_name'] = $dbname;
		$_SESSION['db'] = $_SESSION['mongo']->selectDB($dbname);
	}
	return safe_session('db_name', '');
}


/**
* select collection
* return name of current collection only
* if empty then return name of current collection only
*
* @param mixed $collection
*/
function select_collection($collection = '') {
	$old_name = safe_session('collection_name', '');
	if ($collection !== '' && $collection !== $old_name) {
		$_SESSION['collection_name'] = $collection;
		$_SESSION['collection'] = $_SESSION['db']->selectCollection($collection);
	}
	return $old_name;
}

/**
* my own print function
*
* @param mixed $os
*/
function o($os) {
	echo print_r($os, true) . "<br>";
}



/**
* simplexml error handler
*
* @param mixed $error
* @param mixed $xml
*/
function display_xml_error($error, &$xml)
{
	$return  = $xml[$error->line - 1] . "\n";
	$return .= str_repeat('-', $error->column) . "^\n";

	switch ($error->level) {
		case LIBXML_ERR_WARNING:
			$return .= "Warning $error->code: ";
			break;
		case LIBXML_ERR_ERROR:
			$return .= "Error $error->code: ";
			break;
		case LIBXML_ERR_FATAL:
			$return .= "Fatal Error $error->code: ";
			break;
	}

	$return .= trim($error->message) .
	"\n  Line: $error->line" .
	"\n  Column: $error->column";

	if ($error->file) {
		$return .= "\n  File: $error->file";
	}

	return "$return\n\n--------------------------------------------\n\n";
}




//TODO: hmmm an (optional) autoincrement counter for dynlists may be fine ?! (eg. to find top-10 of a references value)
// using my_array_search_array() with array('v' => ...)
/**
* dynamically insert ref string to document and dyntable (to create filter lists or normalized refs)
*
* @param mixed $doc
* @param mixed $xml
* @param mixed $key
* @param mixed $dynlists
* @param mixed $prefix affects name of dynlist stored collection name
* @param mixed $map affects doc name
* @param mixed $setref if set then ref id is set in doc (instead of value)
* @param mixed $setid if set then uses that id instead of searching
* @param mixed $split if set then value will be splitted into parts by given string (eg. '/' will split 'Comedy/Horror' into two individual entries in doc and 2 entries in dynlists !will not work in conjunction with $setid!
* @param mixed $map_values if set then values will be replaced by values within array
* @param mixed $join if set then value will be joined from parts by given array (eg. ', ' will join ['Comedy', 'Horror'] into 'Comedy, Horror')
*/
function dynset_string(&$doc, &$xml, $key, &$dynlists, $prefix = '', $map = false, $setref = false, $setid = false, $split = false, &$map_values = false, $join = false) {
	if (($v = (string)$xml->$key) != '') {	// if key is set in xml
		$key = ($map ? $map : $key);	// map key
		$dkey = $prefix . $key;	// prefix for dynlists array name
		if (!isset($dynlists[$dkey])) {	// init as array if not exists
			$dynlists[$dkey] = array();
		}
		$dldk = &$dynlists[$dkey];	// ref for faster access
		if ($setid === false) {	// find and possibly create index by own
			if ($split === false) {
				$va = array($v);
			} else {	// possibly split value into parts
				$va = explode($split, $v);
			}
			if ($setref === true) {	// for setref store keys in array in case of split
				$ka = array();
			}
			foreach ($va as $i => $val) {
				if ($map_values) {	// map dynlist entry also
					$val = safe_array($map_values, $val, $val);
					$va[$i] = $val;
				}
				$k = array_search($val, $dldk, true);	// search for value in dynlists array
				if ($k === false) {	// not found
					$dldk[] = $val;	// append value with new index
					if ($setref === true) {	// if new index will be ref in doc then calculate index (not using count)
						$ak = array_keys($dldk);
						$k = end($ak);	// get "real" last index (may NOT be count if some are deleted in between)  ! "$k = count($dynlists[$key]) - 1;" may be wrong)
					}
				}
				if ($setref === true) {	// for setref store keys in array in case of split
					$ka[] = $k;
				}
			}
		} else {	// when setid then no split!
			$dldk[$setid] = $v;	// use given index
			$va = array($v);
		}
		if ($setref === true) {
			if (count($ka) > 1) {
				$doc[$key] = $ka;
			} else {
				$doc[$key] = $k;	// to have relational value (not good for noSQL) - we dont want this! (or?) [when so then we need heavily use of map/reduce for doing JOIN like things] ... but were using noSQL !!
			}
		} else {
			if ($join !== false) {
				$doc[$key] = implode($join, $va);
			} else {
				if (($split !== false) || (count($va) > 1)) {	// insert array of more than one entry or even if split ist set (to create array always)
					$doc[$key] = $va;
				} else {
					//				if ($map_values) {	// map entry within doc
					//					$v = safe_array($map_values, $v, $v);
					//				}
					$doc[$key] = $va[0];	// insert value for key directly to document (and only have relational collections to have an list of available entries ) --- GREAT! (used for dyn. Filters)
				}
			}
		}
	}
}


/**
* set string in doc if present in xml
*
* @param docref $doc
* @param xmlref $xml
* @param string $key
* @param string $map
*/
function set_string(&$doc, &$xml, $key, $map = false) {
	if ($xml->$key != '') $doc[($map ? $map : $key)] = (string)$xml->$key;
}


/**
* set int in doc if present in xml
*
* @param docref $doc
* @param xmlref $xml
* @param string $key
* @param string $map
*/
function set_int(&$doc, &$xml, $key, $map = false) {
	if ($xml->$key != '') $doc[($map ? $map : $key)] = (int)$xml->$key;
}


/**
* set variable boolean (from string) in doc if present in xml
*
* @param docref $doc
* @param xmlref $xml
* @param string $key
* @param string $true
* @param string $map
*/
function set_bool(&$doc, &$xml, $key, $true = 'true', $map = false) {
	if ($xml->$key != '') $doc[($map ? $map : $key)] = (boolean)($xml->$key == $true);	// using "==" instaed of "===" ... or using (string)-cast on object ...
}


/**
* read known dynlists from db
*
* @param mixed $dynlists
*/
function read_dynlists(&$dynlists) {
	// read list of dynlists
	$saved_collection_name = select_collection(_cfg('collection_dynlists'));	// and save current collection
	$cursor = safe_session('collection')->find();
	while ($cursor->hasNext() ) {
		$d = $cursor->getNext();
		$dynlists[(string)$d['v']] = array();
	}
	// read known dynlists into array
	foreach ($dynlists as $name => $values) {
		select_collection(_cfg('collection_dynlists_prefix') . $name);
		$cursor = safe_session('collection')->find();
		while ($cursor->hasNext() ) {
			$d = $cursor->getNext();
			$dynlists[$name][] = (string)$d['v'];
		}
	}
	select_collection($saved_collection_name);	// restore collection from entry point
}


/**
* write dynlists to db
*
* @param mixed $dynlists
*/
function write_dynlists(&$dynlists) {
	// save names of dynlists in separate collection
	$saved_collection_name = select_collection(_cfg('collection_dynlists'));	// and save current collection
	$ops = array( 'upsert' => true, 'multi' => false );
	$i = 0;
	foreach ($dynlists as $name => $values) {
		$query = array('_id' => $i);
		$insert = array('_id' => $i, 'v' => $name);
		$r = safe_session('collection')->update($query, $insert, $ops);
		$i++;
	}
	// save content of dynlists into separate collections
	$ops = array( 'upsert' => true, 'multi' => false );
	foreach ($dynlists as $name => $values) {
		select_collection(_cfg('collection_dynlists_prefix') . $name);
		foreach ($values as $index => $value) {
			$query = array('_id' => $index);
			$insert = array('_id' => $index, 'v' => $value);
			$r = safe_session('collection')->update($query, $insert, $ops);
		}
	}
	select_collection($saved_collection_name);	// restore collection from entry point
}


/**
* search array for array part
* eg.:
* $a = array(0 => array('v' => "ok", 'c' => 123), 1 => array('v' => "not", 'c' => 234), 2 => array('v' => "und", 'c' => 345));
* $b = array('v' => 'und');
* $f = my_array_search_array($a, $b);
* will give $f = 2;
*
* @param array $a
* @param array $b
*/
function my_array_search_array(&$a, &$b) {
	$bk = array_keys($b);
	$bk = $bk[0];
	$bv = $b[$bk];
	$f = false;
	foreach ($a as $k => $v) {
		if ($v[$bk] == $bv) {
			$f = $k;
			break;
		}
	}
	return $f;
}
// TEST:
//$a = array(0 => array('v' => "ok", 'c' => 123), 1 => array('v' => "not", 'c' => 234), 2 => array('v' => "und", 'c' => 345));
//$b = array('v' => 'not');
//$f = my_array_search_array($a, $b);
//echo("a=");print_r($a);echo("<br>");
//echo("b=");print_r($b);echo("<br>");
//echo("f=");print_r($f);echo("<br>");
//die();


/**
* set progress into 'progress'-collection with given progress-id an percentage
*
* @param int $id
* @param int $percent (-1 to init start (will be 0%))
* return true on success
*/
function setprogress($id = -1, $percent, $infos = array()) {
	$return = false;
	if ($id > 0 && $percent >= -1 && $percent <= 100) {
		$save_collection = select_collection('progress');
		$query = array('_id' => $id);
		$vals = array();
		switch ($percent) {
			case -1:
				$vals['start'] = date('Y-m-d H:i:s');
				$vals['end'] = '';
				$vals['update'] = '';
				$percent = 0;
				break;
			case 100:
				$vals['end'] = date('Y-m-d H:i:s');
				$vals['update'] = '';
				break;
			default:
				$vals['update'] = date('Y-m-d H:i:s');
				break;
		}
		$vals['v'] = $percent;
		$vals['infos'] = $infos;
		$update = array('$set' => $vals);
		$option = array( 'upsert' => true, 'multi' => false );
		$r = safe_session('collection')->update($query, $update, $option);
		select_collection($save_collection);
		$return = true;
	}
	return $return;
}

/**
* read progress from 'progress'-collection with given progress-id
*
* @param int $id
* return array($result{true|false}, v(0..100))
*/
function getprogress($id = -1) {
	$v = -1;
	$result = false;
	if ($id > 0) {
		$query = array('_id' => $id);
		$projection = array('_id' => 0, 'v' => 1);
		$save_collection = select_collection('progress');
		$cursor = safe_session('collection')->find($query, $projection);
		while ($cursor->hasNext()) {
			$result = true;
			$o = $cursor->getNext();
			$v = (int)($o['v']);
			break;	// only the one and only possible
		}
		select_collection($save_collection);
	}
	return array($result, $v);
}

?>
