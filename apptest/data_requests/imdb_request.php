<?php
	require_once('../libs/classes/no_sql.php');
	$no_sql = new No_SQL();

	//echo '<pre>';
    $cursor = $no_sql->no_sql_find();
    $result_array = array();
    foreach($cursor as $n_key) {
               array_push($result_array, $n_key);
    }
    $result_array = json_encode($result_array);

    header("Content-type: application/json");
    echo '{"data":' . $result_array . '}';
?>
