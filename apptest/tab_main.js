// simply build tab structure
$(document).ready(function () {

	// TEST: dynamically create tabs with jquery [done BEFORE jqxTabs initialized]
	$("#ul_tabs").append('<li id="li_tab_dynMade" style="margin-left: 10px;">dynMade</li>');
	$("#jqxTabs").append('<div id="tab_dynMade">test ... dynamisch erzeugter Tab ...</div>');

	// Create jqxTabs
	$('#jqxTabs').jqxTabs({ width: '100%', height: '750px', position: 'top', selectionTracker: true, animationType: 'fade', enableDropAnimation: true, reorder: true, theme: selectedTheme});
	$('#settings div').css('margin-top', '10px');

	// TEST: dynamically create tabs with jqxtabs [done AFTER jqxTabs initialized]
	$("#jqxTabs").jqxTabs('addLast', 'dynMade2', '<div id="tab_dynMade2">test ... dynamisch erzeugter Tab 2 ...</div>');

	// set 1st tabs as default
	$('#jqxTabs').jqxTabs('select', 0);

	$("#jqxTabs").css('visibility', 'visible').hide().fadeIn(2000);
//	$("#jqxTabs").css('visibility', 'visible').hide().show(2000);
});



// some tests ... IGNORE !
$(document).ready(function () {
	//TEST: try to offer content as download from js only ...
	if (false) {	// It works !?
		window.URL = window.URL || window.webkitURL;
		var a = document.createElement('a');
		//		var blob = new Blob(['inhalt blablabla'], {'type': 'application\/octet-stream'});
		var blob = new Blob(['inhalt blablabla'], {'type': 'plain/text', endings: 'transparent'});
		a.href = window.URL.createObjectURL(blob);
		a.download = 'filename.txt';
		a.click();
	}
});
$(document).ready(function () {
	//	$('#jqxTabs').jqxTabs('val', 3);
});

//# sourceURL=tab_main.js