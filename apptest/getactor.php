<?php
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_movies'));


$count = 0;

// memory and time ...
$before = memory_get_usage(true);
$time_start = microtime(true);

$data = array();

$searchStringPart = safe_request('searchStringPart');
// test: ok : write query in json ... better to read than php-array
$query_json = '{
  "persons": {
    "$elemMatch": {
      "name": {
        "$regex": "' . $searchStringPart . '",
        "$options": "i"
      },
      "function": "Actor"
    }
  }
}';
//        "$regex": "^' . $name_startsWith . '.*",

$query = json_decode($query_json);
//$projection_json = '{
//  "_id": 1,
//  "title": 1,
//  "original_title": 1,
//  "persons.$": 1
//}';	// only give back found entry of array!
$projection_json = '{
  "persons.$": 1
}';	// only give back found entry of array!
$projection = json_decode($projection_json);

//$cursor = safe_session('collection')->find($query, $projection)->sort($sort)->skip($skip)->limit($limit);
$cursor = safe_session('collection')->find($query, $projection);
while ($cursor->hasNext() ) {
	$count++;
	$d = $cursor->getNext();
	$x = $d['persons'][0]['name'];
	$data[] = $x;
}
//$d = safe_session('collection')->findOne($query, $projection);
//$data[] = $d['persons'][0]['name'];

// may be the following is possible in mongo too ?!
$x = array_unique($data);
sort($x);
$data = array();
$maxcount = safe_request('maxRows', 99);
$c = 0;
foreach ($x as $k => $v) {
	$data[] = array('name' => $v);
	$c++;
	if ($c == $maxcount) {
		break;
	}
}
$count = count($data);

// memory and time ...
$time_end = microtime(true);
$time = $time_end - $time_start;
$after = memory_get_usage(true);

header("Content-type: application/json");
echo "{\"data\":" .json_encode($data). ", \"count\": $count}";

/* filme mit sandra bullock in diesem jahr
{
'person' : { $elemMatch: {name: "Sandra Bullock"}}
// ,laufzeit : {$gt: 0}
,cinema_release_date: {$gt: "2013-01-01"}
}
*/

/* filme mit darsteller sandra-irgendwas-ock
{
  "persons": {
    "$elemMatch": {
      "name": {
        "$regex": "sandra.*ock",
        "$options": "i"
      },
      "function": "Actor"
    }
  }
}
*/
?>
