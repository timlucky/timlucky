
//////////////////////////////////////////////////
// LOAD OTHER RESOURCES BEFORE:
jQuery.cachedScript = function (url, options) {
	// allow user to set any option except for dataType, cache, and url
	options = $.extend(options || {}, {
		dataType: "script",
		cache: true,
		async: false,
		url: url
	});

	// Use $.ajax() since it is more flexible than $.getScript
	// Return the jqXHR object so we can chain callbacks
	return jQuery.ajax(options);
};

/**
*
* @param {type} url
* @returns {undefined}
*/
function loadjs(url) {
	$.cachedScript(url).always(function (script, textStatus) {
		console.log("loaded script : '" + url + "' : '" + textStatus + "'");
	});
}

/**
*
* @param {type} url
* @param {type} option
* @returns {undefined}
*/
function loadcss(url, option) {
	$('head').append($('<link rel="stylesheet" ' + option + '>').attr('href', url));
	console.log("loaded stylesheet : '" + url + "'");
}

// load js and css dynamically !
//loadcss('../../jqx/jqwidgets/styles/jqx.base.css', 'id="basetheme"');
loadcss('css/main.css', 'id="maincss"');
loadjs('../../jqx/jqwidgets/jqx-all.js');
//loadjs('../../jqx/jqwidgets/jqxdata.export.js');
loadjs('tools.js');
loadjs('jquery.timers.js');
loadjs('tab_main.js');
loadjs('tab_tests.js');
loadjs('tab_showallgrid.js');
loadjs('tab_scanner.js');
loadjs('tab_filmstarts_scanner.js');
loadjs('tab_import.js');

//loadcss('../../jqx/jqwidgets/styles/jqx.arctic.css', 'id="theme"');	// option necessary for theme switcher! (not yet)


var selectedTheme = 'arctic';

var themes = [
	{ label: 'Arctic', group: 'Themes', value: 'arctic' },
	{ label: 'Web', group: 'Themes', value: 'web' },
	{ label: 'Bootstrap', group: 'Themes', value: 'bootstrap' },
	{ label: 'Metro', group: 'Themes', value: 'metro' },
	{ label: 'Metro Dark', group: 'Themes', value: 'metrodark' },
	{ label: 'Office', group: 'Themes', value: 'office' },
	{ label: 'Orange', group: 'Themes', value: 'orange' },
	{ label: 'Fresh', group: 'Themes', value: 'fresh' },
	{ label: 'Energy Blue', group: 'Themes', value: 'energyblue' },
	{ label: 'Dark Blue', group: 'Themes', value: 'darkblue' },
	{ label: 'Black', group: 'Themes', value: 'black' },
	{ label: 'Shiny Black', group: 'Themes', value: 'shinyblack' },
	{ label: 'Classic', group: 'Themes', value: 'classic' },
	{ label: 'Summer', group: 'Themes', value: 'summer' },
	{ label: 'High Contrast', group: 'Themes', value: 'highcontrast' },
	{ label: 'Lightness', group: 'UI Compatible', value: 'ui-lightness' },
	{ label: 'Darkness', group: 'UI Compatible', value: 'ui-darkness' },
	{ label: 'Smoothness', group: 'UI Compatible', value: 'ui-smoothness' },
	{ label: 'Start', group: 'UI Compatible', value: 'ui-start' },
	{ label: 'Redmond', group: 'UI Compatible', value: 'ui-redmond' },
	{ label: 'Sunny', group: 'UI Compatible', value: 'ui-sunny' },
	{ label: 'Overcast', group: 'UI Compatible', value: 'ui-overcast' },
	{ label: 'Le Frog', group: 'UI Compatible', value: 'ui-le-frog' }
];

$(document).ready(function () {	// or simply use this: $(function () {}

	$("#headerpanel").jqxPanel({width: '100%', height: 100, sizeMode: "fixed", scrollBarSize: 0, theme: selectedTheme});

	function initthemes(initialurl) {
		var loadedThemes = [0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
		var me = this;
		this.$head = $('head');
		$('#themeComboBox').jqxDropDownList({ source: themes, theme: selectedTheme, selectedIndex: 0, dropDownHeight: 200, width: '140px', height: '20px' });

		var hasParam = window.location.toString().indexOf('?');
		if (hasParam != -1) {
			var themestart = window.location.toString().indexOf('(');
			var themeend = window.location.toString().indexOf(')');
			var theme = window.location.toString().substring(themestart + 1, themeend);
			$.data(document.body, 'theme', theme);
			selectedTheme = theme;
			var themeIndex = 0;
			$.each(themes, function (index) {
				if (this.value == theme) {
					themeIndex = index;
					return false;
				}
			});
			$('#themeComboBox').jqxDropDownList({ selectedIndex: themeIndex });
			loadedThemes[0] = -1;
			loadedThemes[themeIndex] = 0;
		}
		else {
			$.data(document.body, 'theme', selectedTheme);
		}

		$('#themeComboBox').on('select', function (event) {
			setTimeout(function () {
				var selectedIndex = event.args.index;
				//            var selectedTheme = '';
				var url = initialurl;

				var loaded = loadedThemes[selectedIndex] != -1;
				loadedThemes[selectedIndex] = selectedIndex;

				selectedTheme = themes[selectedIndex].value;
				url += selectedTheme + '.css';

				if (!loaded) {
					if (document.createStyleSheet != undefined) {
						document.createStyleSheet(url);
					}
					else me.$head.append('<link rel="stylesheet" href="' + url + '" media="screen" />');
				}
				$.data(document.body, 'theme', selectedTheme);
				// 	location.reload();

//TODO: try to find way to change property theme of all jqx objects ...
				$("#headerpanel").jqxPanel({theme: selectedTheme});
				$('#themeComboBox').jqxDropDownList({theme: selectedTheme});
				$('#jqxTabs').jqxTabs({ theme: selectedTheme});
				$('#chartWindow').jqxWindow({theme: selectedTheme});	// but theme isnt applied to jqchart because its SVG
//$('#jqxWidget').each( function() { $(this).attr('theme', selectedTheme); });

				//				$.jqx = $.jqx || {};
				//				$.jqx.theme = selectedTheme;

				if (true) {
					var theme = selectedTheme;
					$.data(document.body, 'theme', theme);
					selectedTheme = theme;
					var themeIndex = 0;
					$.each(themes, function (index) {
						if (this.value == theme) {
							themeIndex = index;
							return false;
						}
					});
					//					$('#themeComboBox').jqxDropDownList({ selectedIndex: themeIndex });
					loadedThemes[0] = -1;
					loadedThemes[themeIndex] = 0;
				}
				}, 5);
		});
	};

	initthemes("../../jqx/jqwidgets/styles/jqx.");
});
