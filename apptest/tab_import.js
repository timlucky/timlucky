var gettitles_source;
var gettitles_dataAdapter;

// show "please wait" in div
function please_wait_import_result() {
	$("#import_result").html('please wait ...');
}

// callback function to display data from ajax
function callback_import_result(data) {
	var jsonPretty = JSON.stringify(data, null, 2);
	$("#import_result").html("<pre>" + jsonPretty + "</pre>");
//	console.log('callback_import_result: ' + jsonPretty);
}

function myprogress(v) {
	console.log("myprogress: v=" + v);
	if (v >= 0 && v <= 100 && $('#jqxprogressbar').jqxProgressBar('val') != v) {	// set new if valid and changed
		console.log("myprogress: set value to progressbar v=" + v);
		$('#jqxprogressbar').jqxProgressBar('val', v);	// att: $('#...').val(55) seems not to work anymore ?!?
	}
}

$(function() {
	// Create jqxPanel
	$("#import_result_panel").jqxPanel({width: 700, height: 500, sizeMode: "fixed", scrollBarSize: 20, autoUpdate: true, theme: selectedTheme});
});

$(function() {
	// handle jqxProgressBar
	$("#jqxprogressbar").jqxProgressBar({value: 0, width: 400, height: 20, animationDuration: 1000, showText: true, theme: selectedTheme});
	$("#jqxprogressbar").hide();
	$('#jqxprogressbar').on('complete', function(event) {	// stop timer on complete
		$("#jqxprogressbar").fadeOut("slow", function() {});
		$(document).stopTime('full_xml_import');
		$(document).oneTime(500, 'update_xml_import', function() {	// update search
			gettitles_dataAdapter = new $.jqx.dataAdapter(gettitles_source);
			$("#jqxInput_title").jqxInput({source: gettitles_dataAdapter});
			$("#jqxprogressbar").jqxProgressBar({value: 0});
		});
	});
	$("#jqxprogressbar").jqxTooltip({content: 'will disappear after 100% reached ...', position: 'mouse', name: 'jqxprogressbar_tooltip', theme: selectedTheme});

	// full xml import button
	$("#full_xml_import_button").jqxButton({width: '150', theme: selectedTheme});
	$("#full_xml_import_button").on('click', function() {
		please_wait_import_result();
		$("#jqxprogressbar").fadeIn("slow", function() {});
		$(document).stopTime('full_xml_import');	// stop timer to ensure only one is running (in case you hit button more than once ...)
		$(document).everyTime(2500, 'full_xml_import', function() {
			getprogress(1, myprogress);
		});
		execPHPf('import_movies.php?use_xml_update=0', callback_import_result);
	});
	$("#full_xml_import_button").jqxTooltip({ content: '<b>Info:</b> will take 90 seconds to<br>fetch ~81700 movies (450 MB of data)<br>to build database and dynlists<br><i>(please be patient)</i>', position: 'mouse', name: 'import_button_tooltip', theme: selectedTheme});

	// update xml import button
	$("#update_xml_import_button").jqxButton({width: '150', theme: selectedTheme});
	$("#update_xml_import_button").on('click', function() {
		please_wait_import_result();
		$("#jqxprogressbar").fadeIn("slow", function() {});
		$(document).stopTime('full_xml_import');	// stop timer to ensure only one is running (in case you hit button more than once ...)
		$(document).everyTime(1500, 'full_xml_import', function() {
			getprogress(1, myprogress);
		});
//		execPHPf('import_movies.php?use_xml_update=0', callback_import_result);
		execPHPf('import_movies.php', callback_import_result);
//		$(document).oneTime(2500, 'update_xml_import', function() {	// update search
//			gettitles_dataAdapter = new $.jqx.dataAdapter(gettitles_source);
//			$("#jqxInput_title").jqxInput({source: gettitles_dataAdapter});
//		});
	});
	$("#update_xml_import_button").jqxTooltip({ content: '<b>Info:</b> will take about a second for daily update<br>import per 100 newly and updated movies<br><i>(please be patient)</i>', position: 'mouse', name: 'import_button_tooltip', theme: selectedTheme});
});

//# sourceURL=tab_import.js
