<?php
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_genre'));

$count = 0;

$query = array();
$projection = array('_id' => 0, 'v' => 1);
$sort = array('v' => 1);

$data = array();

$cursor = safe_session('collection')->find($query, $projection)->sort($sort);
if ($cursor) {
	while ($cursor->hasNext() ) {
		$count++;
		$d = $cursor->getNext();
		$data[] = $d;
	}
}

header("Content-type: application/json");
echo "{\"data\":" .json_encode($data). ", \"count\": $count}";
?>
