<?php
require_once('config.php');
require_once('tools.php');


open_database(_cfg('database_name'), _cfg('collection_movies'));

$count = 0;

// memory and time ...
$before = memory_get_usage(true);
$time_start = microtime(true);

// print out db
$data = array();

$sort = array('_id' => 1);	// order of entered in db
//$query = array('cinema_release_date' => array('$gte' => '2012-01-01'), 'charts_germany.visitors_overall' => array('$gt' => 0));
$query = array('cinema_release_date' => array('$gte' => '2012-01-01'));
//$projection = array('_id' => 1, 'title' => 1, 'cinema_release_date' => 1, '3d' => 1, 'boxoffice_gross_germany' => 1, 'charts_germany.visitors_overall' => 1, 'persons.director' => 1, 'persons.top4actors' => 1, 'synopsisshort' => 1, 'genres' => 1);
$projection = array('_id' => 1, 'title' => 1, 'cinema_release_date' => 1, '3d' => 1, 'boxoffice_gross_germany' => 1, 'charts_germany.visitors_overall' => 1, 'director' => 1, 'top4actors' => 1, 'synopsisshort' => 1, 'genres' => 1);
//$projection = array('_id' => 1, 'title' => 1, 'persons.director' => 1, 'persons.top4actors' => 1);

//TODO: jetzt probieren: aus dem array "persons" die ersten 4 personen die "rolle_funktion" = "Darsteller" haben als group_concat in das feld "darstellertop4" ausgeben (wird wohl map&reduce??)
// ... ok, wenn immer gewuenscht, dann doch gleich beim import als eigenen tag anlegen ... spart doch viel arbeit !? (scheiss auf speicherplatz!)

$skip = 0;
$limit = 0;
$cursor = safe_session('collection')->find($query, $projection)->skip($skip)->limit($limit);
while ($cursor->hasNext() ) {
	$count++;
	$d = $cursor->getNext();
	$data[] = $d;
}

// get all entries count
$s = safe_session('db')->command(array('collStats' => _cfg('collection_movies')));
$allcount = $s['count'];

// memory and time ...
$time_end = microtime(true);
$time = $time_end - $time_start;
$after = memory_get_usage(true);

$total_rows = $count;

header("Content-type: application/json");
echo "{\"data\":" .json_encode($data). ", \"count\": $count, \"allcount\": $allcount, \"TotalRows\": $total_rows}";

?>
