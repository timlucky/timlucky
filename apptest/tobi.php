<?php

/**
* blabla
*
* @param mixed $wert
*/
function main($wert = 0) {
	$os = array();
	$os[] = "mein test";
	$os[] = "und so weiter";

	$osr = array();

	$osr[0] = array("wert" => $wert, "und noch was" => 34567);

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	// WORKING ...

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os[] = "needs $time seconds";
	$after = memory_get_usage(true);
	$os[] = "memory used = " . (int)(($after - $before)/1024/1024 + 0.999) . " Mbytes";

	$oa = array('result' => true, 'info' => $os, 'values' => $osr);

	return $oa;
}


////////
require_once('config.php');
require_once('tools.php');

$collection = 'tobi';	// -> config !!
open_database(_cfg('database_name'), $collection);

$wert = (int)safe_request('wert', 0);

$r = main($wert);
echo json_encode($r);

?>
