<?php

/**
* main function
*
*/
function main($search_id = 0) {
	$os = array();

	$count = 0;

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	// print out db
	$osr = array();
		// filme mit mehr als 1 mio besuchern an irgendeinem wochenende
		$sort = array('_id' => 1);	// order of entered in db
//		$cursor = $_SESSION['collection']->find(array('charts_germany.chart' => array( '$elemMatch' => array('visitors_we' => array('$gte' => 1000000)))), array('id' => 1, 'titel' => 1, 'charts_germany' => 1))->sort($sort);
//		$cursor = $_SESSION['collection']->find(array('titel' => 'Gravity'), array('id' => 1, 'titel' => 1, 'cinema_release_date' => 1))->sort($sort);
		if ($search_id != 0) {
			$query = array('_id' => $search_id);
		} else {
			$query = array('title' => 'Gravity');
		}
//		$projection = array('_id' => 1, 'title' => 1, 'persons.director' => 1);
		$projection = array();
		$cursor = safe_session('collection')->find($query, $projection)->sort($sort);
		while ($cursor->hasNext() ) {
			$count++;
			$osr[] = $cursor->getNext();
		}

	$os[] = "found $count movies";

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os[] = "needs $time seconds";
	$after = memory_get_usage(true);
	$os[] = "memory used = " . (int)(($after - $before)/1024/1024 + 0.999) . " Mbytes";

	$oa = array('result' => true, 'info' => $os, 'values' => $osr);

	return $oa;
}


////////
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_movies'));

$search_id = (int)safe_request('_id', 0);

$r = main($search_id);
echo json_encode($r);
?>
