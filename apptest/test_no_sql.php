<?php
    require_once('libs/classes/no_sql.php');

    $no_sql = new No_SQL();

    //TEST No_SQL->no_sql_find()
	//$select = array('id');
	echo '<pre>';
    $cursor = $no_sql->no_sql_find();
    $result_array = array();
    foreach($cursor as $n_key) {
               //array_push($result_array, $n_key);
               //print_r($n_key);
               echo $n_key['title'] . '<br>';
               $a = 'http://www.mediabiz.de/suche/result?searchCategories=kinofilm&t=Der Hobbit - Smaugs Einoede&selectedAreaByResult=&orderResult=sortrelevance-0&itemCounter=25&fullVersion=1';
    }
    //$result_array = json_encode($result_array);
    print_r($result_array);
	echo '</pre>';

    //TEST No_SQL->no_sql_find_one($pConditions)

    $condition = array("user" => 'demo@9lessons.info',"password" => md5("demo_password"));
    $select = array('id');
    $result = $no_sql->no_sql_find_one($condition, $select);
    if ($result) {
        //echo $result['user'];
        //print_r($result);
        foreach($result as $n_key) {
                //var_dump($n_key);
        }
    } else {
        echo 'NO RESULT FOUND!';
    }

    //TEST No_SQL->insert()

    $insert = array("user" => "object_test@no_sql.obj", "password" => 'Test Password');
    $no_sql->insert($insert);


    //TEST No_SQL->update()

    $update = array('$set' => array("user" => "UPDATED_object_test@no_sql.obj"));
    $where = array("user" => "NEXT_UPDATED_object_test@no_sql.obj", "password" => 'Test Password');
    $no_sql->update($where, $update);


    //TEST No_SQL->remove()

    $where = array("user" => "UPDATED_object_test@no_sql.obj", "password" => 'Test Password');
    $no_sql->remove($where);

?>
