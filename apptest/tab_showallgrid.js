var chart_id = -1;
var chart_settings;
var chart_source;
var chart_dataAdapter;

function showgrid() {

	// prepare the data
	var showall_source = {
		datatype: "json",
		datafields: [
			{name: '_id', type: 'number'},
			{name: 'title', type: 'string'},
			{name: 'cinema_release_date', type: 'date'},
			{name: 'boxoffice_gross_germany', type: 'number'},
			{name: 'visitors_overall', map: 'charts_germany>visitors_overall', type: 'number'},
			{name: '3d', type: 'bool'},
			//					{name: 'director', map: 'persons>director', type: 'string'},
			{name: 'director', map: 'director', type: 'string'},
			//					{name: 'top4actors', map: 'persons>top4actors', type: 'string'},
			{name: 'top4actors', map: 'top4actors', type: 'string'},
			//					{name: 'genre', map: 'genre', type: 'array', separator: '| '},	// separator or format not working ... always ','
			{name: 'genre', map: 'genres', type: 'string'},
			{name: 'synopsisshort', type: 'string'}
		],
		id: 'id',
		url: "getalldata.php",
		processdata: function (data) {
			//						data.maxRows = 50;
			console.log("process data");
		},
		pager: function (pagenum, pagesize, oldpagenum) {
			// callback called when a page or page size is changed.
			console.log("pager: pagenum=" + pagenum + " pagesize=" + pagesize + " oldpagenum=" + oldpagenum);
		},
		//					beforeprocessing: function (data) {
		//						showall_source.totalrecords = data.TotalRows;
		//					},
		//					sort: function () {
		// update the grid and send a request to the server.
		//						$("#jqxgrid").jqxGrid('updatebounddata');
		//					},
		root: 'data'
	};

	var showall_dataAdapter;
	showall_dataAdapter = new $.jqx.dataAdapter(showall_source, {
		downloadComplete: function (data, status, xhr) {
			//								console.log(print_r(data));
			//								console.log(print_r(data.count));
			$('#count').html("" + data['allcount'] + " entries in database ... showing " + data['count'] + " of them (" + (data.data.length) + ")");
		},
		loadComplete: function (data) {
			//					$("#jqxgrid").jqxGrid('autoresizecolumns');
		},
		loadError: function (xhr, status, error) {
			console.log("loadError");
		}
	});

	var columnrenderer = function (value) {
		return '<div style="text-align: center; font-style:italic;">' + value + '</div>';
	}

	var headerTooltipRenderer = function(text) {
		return function (columnHeaderElement) {
			return "<div class='renderedColumn' style='margin-left: 5px; margin-top: 5px;' title='" + text + "'>" + columnHeaderElement + "</div>";
		};
	};
	var xheaderTooltipRenderer = function (columnHeaderElement) {
		var s = columnHeaderElement;
		return "<div style='margin-left: 10px; margin-top: 5px;' title='" + s.replace(/(<([^>]+)>)/ig,"") + "'>" + s + "</div>";
	};
	$("div .renderedColumn").each(function() { this.parentElement.title = this.title;});

	$("#jqxgrid").jqxGrid({
		theme: selectedTheme,
		width: 1100,
		//				width: '90%',
		height: '80%',
		columnsheight: 50,
		source: showall_dataAdapter,
		showfilterrow: true,
		filterable: true,
		sortable: true,
		//				pageable: true,
		//				autoheight: true,
		autoshowfiltericon: true,
		columnsresize: true,
		columnsreorder: true,
		//							virtualmode: true,
		//							rendergridrows: function () {
		//								return showall_dataAdapter.records;
		//							},
		//				selectionmode: 'singlerow',
		selectionmode: 'singlecell',
		showaggregates: true,
		showstatusbar: true,
		statusbarheight: 50,
		columns: [
			//					{ text: 'ID', datafield: '_id', cellsalign: 'right', width: 75, renderer: columnrenderer },
			{text: 'Title', datafield: 'title', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 300, pinned: true, renderer: headerTooltipRenderer('(column fixed only for demonstration)')},
			{text: 'Cinema Release', datafield: 'cinema_release_date', filtertype: 'date', width: 210, cellsalign: 'right', cellsformat: 'dddd d MMM yyyy-MM-dd'},
			{text: 'Box-office gross<br/>Germany €', datafield: 'boxoffice_gross_germany', filtertype: 'number', cellsalign: 'right', width: 120, aggregates: ['sum', 'avg'], cellsformat: 'n2', renderer: headerTooltipRenderer('...')},
			{text: 'Visitors overall<br/>Germany', datafield: 'visitors_overall', filtertype: 'number', cellsalign: 'right', width: 120, aggregates: ['sum', 'avg'], cellsformat: 'n2', renderer: headerTooltipRenderer('click on value to get chart')},
			{text: '3D', datafield: '3d', columntype: 'checkbox', filtertype: 'bool', width: 60
				,aggregates: [{ '3D':
					function (aggregatedValue, currentValue) {
						if (currentValue) {
							return aggregatedValue + 1;
						}
						return aggregatedValue;
					}
					},
					{ '2D':
						function (aggregatedValue, currentValue) {
							if (!currentValue) {
								return aggregatedValue + 1;
							}
							return aggregatedValue;
						}
					}
				]
			},
			{text: 'Genre', datafield: 'genre', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 150},
			//					{text: 'Genre', datafield: 'genre', filtertype: 'checkedlist', filteritems: ['Comedy', 'Action', 'Thriller', 'Drama'], filtercondition: 'CONTAINS', width: 150},
			{text: 'Director', datafield: 'director', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 140},
			{text: 'Top 4 Actors', datafield: 'top4actors', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 500},
			{text: 'Synopsis Short', datafield: 'synopsisshort', columntype: 'textbox', filtertype: 'textbox', filtercondition: 'CONTAINS', width: 800}
		]
	});
	$('#clearfilteringbutton').jqxButton({ height: 25, theme: selectedTheme});
	$('#clearfilteringbutton').click(function () {
		$("#jqxgrid").jqxGrid('clearfilters');
	});

	$("#excelExport").jqxButton({ theme: selectedTheme });
	$("#excelExport").click(function () {
		$("#jqxgrid").jqxGrid('exportdata', 'xls', 'jqxGrid', true, null, true, 'download.php');
	});

	$('#events').jqxPanel({ width: 300, height: 300, theme: selectedTheme});

	var selected_row = null;
	//			$("#jqxgrid").on('rowselect', function (event) {
	$("#jqxgrid").on('cellselect', function (event) {
		var columnfield = event.args.datafield;
		//				if (columnfield == 'boxoffice_gross_germany' || columnfield == 'visitors_overall') {
		if (columnfield == 'visitors_overall') {
			selected_row = event.args.rowindex;
			var data = $('#jqxgrid').jqxGrid('getrowdata', selected_row);
			$('#chartWindow').jqxWindow('open');
			$('#chartWindow').jqxWindow({title: data.title});
			chart_id = data._id;
			//				console.log('');
			//				console.log('---------------------');
			//				console.log('get chart for id=' + chart_id);
			chart_source.data = {_id: chart_id};
			//				console.log("1:dataBind():");
			chart_dataAdapter.dataBind();
			//				console.log("2:ok(databind)");
			if (chart_dataAdapter.records.length > 0) {
				//					console.log("setup chart for id=" + chart_id);
				//					console.log("update:");
				$('#jqxChart').jqxChart('update');
				//					console.log("set...");
				chart_settings.categoryAxis.tickMarksInterval = Math.max(1, Math.round(chart_data_count / 12));
				chart_settings.categoryAxis.unitInterval = Math.max(1, Math.round(chart_data_count / 6));
				chart_settings.categoryAxis.gridLinesInterval = Math.max(1,  Math.round(chart_data_count / 6));
				chart_settings.seriesGroups[0].valueAxis.unitInterval = Math.round(chart_data_visitors_overall_max / 50000) * 50000 / 10;
				chart_settings.seriesGroups[0].valueAxis.maxValue = chart_data_visitors_overall_max * 1.01;

				//					console.log("jqxChart.refresh:");
				$('#jqxChart').jqxChart({showToolTips: true});	// ... because tooltips wont work within jqxwindow (because of wrong z-index ...) [V 3.1.0]
				$('#jqxChart').jqxChart('refresh');
				//					console.log("3:done");
			} else {					// keine daten ...
				console.log("no data for id=" + chart_id);
			}
		}
	});

	//TODO: only when on titles-column!
	//			$("#jqxgrid").jqxTooltip({ content: '<b>Try:</b> click on row to get<br>chart of visitiors in new window', position: 'mouse', name: 'input_tooltip', theme: selectedTheme});

	$("#jqxgrid").on("pagechanged", function (event) {
		//							console.log("pagechanged");
		//				$("#jqxgrid").jqxGrid('autoresizecolumns');
		//							$("#eventslog").css('display', 'block');
		//							if ($("#events").find('.logged').length >= 5) {
		//								$("#events").jqxPanel('clearcontent');
		//							}
		//							var args = event.args;
		//							var eventData = "pagechanged <div>Page:" + args.pagenum + ", Page Size: " + args.pagesize + "</div>";
		//							$('#events').jqxPanel('prepend', '<div class="logged" style="margin-top: 5px;">' + eventData + '</div>');
		//							// get page information.
		//							var paginginformation = $("#jqxgrid").jqxGrid('getpaginginformation');
		//							$('#paginginfo').html("<div style='margin-top: 5px;'>Page:" + paginginformation.pagenum + ", Page Size: " + paginginformation.pagesize + ", Pages Count: " + paginginformation.pagescount + "</div>");
	});
	$("#jqxgrid").on("pagesizechanged", function (event) {
		//							console.log("pagesizechanged");
		//				$("#jqxgrid").jqxGrid('autoresizecolumns');
		//							$("#eventslog").css('display', 'block');
		//							$("#events").jqxPanel('clearcontent');
		//							var args = event.args;
		//							var eventData = "pagesizechanged <div>Page:" + args.pagenum + ", Page Size: " + args.pagesize + ", Old Page Size: " + args.oldpagesize + "</div>";
		//							$('#events').jqxPanel('prepend', '<div style="margin-top: 5px;">' + eventData + '</div>');
		//							// get page information.
		//							var paginginformation = $("#jqxgrid").jqxGrid('getpaginginformation');
		//							$('#paginginfo').html("<div style='margin-top: 5px;'>Page:" + paginginformation.pagenum + ", Page Size: " + paginginformation.pagesize + ", Pages Count: " + paginginformation.pagescount + "</div>");
	});
}

$(document).ready(function () {
	$('#jqxTabs').on('tabclick', function (event) {
		var tabclicked = event.args.item;
		var selectedItem = $('#jqxTabs').jqxTabs('selectedItem');
		console.log('tabclick clicked ' + tabclicked);
		console.log('tabclick selected ' + selectedItem);
		console.log('tabclick title '  + $('#jqxTabs').jqxTabs('getTitleAt', selectedItem));
		if (tabclicked == 1) {	// show grid only when tab opened
	$('#jqxChart').jqxChart(chart_settings);
			showgrid();
		}
	});
	$('#jqxTabs').on('dragStart', function (event) {
		var tabclicked = event.args.item;
		var selectedItem = $('#jqxTabs').jqxTabs('selectedItem');
		console.log('dragStart clicked ' + tabclicked);
		console.log('dragStart selected ' + selectedItem);
		console.log('dragStart title '  + $('#jqxTabs').jqxTabs('getTitleAt', selectedItem));
		if (tabclicked == 1) {	// show grid only when tab opened
			showgrid();
		}
	});

});


var chart_data_visitors_we_max = 0;
var chart_data_visitors_overall_max = 0;
var chart_data_count = 0;

$(document).ready(function () {

	// prepare the data
	chart_source =
	{
		datatype: "json",
		datafields: [
			{ name: 'date_from', txpe: 'date' },
			{ name: 'visitors_we', type: 'number' },
			{ name: 'visitors_overall', type: 'number' }
		],
		root: 'values',
		data: {_id: chart_id},
		url: 'get_charts_de.php'
	};

	chart_dataAdapter = new $.jqx.dataAdapter( chart_source,
		{
			async: false,
			//			autoBind: true,
			downloadComplete: function (data) {
				console.log('downloadComplete');
			},
			loadComplete: function (data) {
				//TODO: grab here precalculated values outside root
				chart_data_visitors_we_max = data.infos.visitors_we_max;
				chart_data_visitors_overall_max = data.infos.visitors_overall_max;
				chart_data_count = data.infos.count;
				console.log('loadComplete');
			},
			loadError: function (xhr, status, error) {
				alert('Error loading "' + source.url + '" : ' + error);
			},
			beforeLoadComplete: function (records) {
				//				console.log(JSON.stringify(records));
				return records;
			}
		}
	);
	//	chart_dataAdapter.dataBind();

	// prepare jqxChart settings
	chart_settings = {
		//		theme: selectedTheme,	// but theme isnt applied to jqchart because its SVG
		title: "Visitors WE / overall",
		description: "---",
		enableAnimations: true,
		showLegend: true,
		padding: { left: 10, top: 5, right: 10, bottom: 5 },
		titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
		source: chart_dataAdapter,
		categoryAxis:
		{
			dataField: 'date_from',
			formatFunction: function (value) {
				var x;
				x = $.jqx.dataFormat.formatdate(value, 'yyyy-MM-dd');
				return x;
			},
			//							toolTipFormatFunction: function (value) {
			//								return value.getDate() + '-' + months[value.getMonth()];
			//							},
			//							type: 'date',
			//							baseUnit: 'week',
			showTickMarks: true,
			tickMarksInterval: 1,
			tickMarksColor: '#888888',
			unitInterval: 1,
			showGridLines: true,
			gridLinesInterval: 1,
			gridLinesColor: '#888888',
			valuesOnTicks: false,
			axisSize: 'auto'
		},
		colorScheme: 'scheme05',
		seriesGroups:
		[
			{
				type: 'line',
				//				type: 'area',
				//				showLabels: true,
				symbolType: 'circle',
				valueAxis:
				{
					unitInterval: 1,
					minValue: 0,
					maxValue: 1,
					displayValueAxis: true,
					description: 'Visitors',
					axisSize: 'auto',
					tickMarksColor: '#888888'
				},
				series: [
					{ dataField: 'visitors_we', displayText: 'Visitors WE' },
					{ dataField: 'visitors_overall', displayText: 'Visitors overall' }
				]
			}
		]
	};

	// init one time (may omit datasource ??)
	//	console.log('initContent: jqxChart(chart_settings) ...');
//	$('#jqxChart').jqxChart(chart_settings);
	//	console.log('initContent: jqxChart(chart_settings) ... DONE');


	$('#chartWindow').jqxWindow({
		theme: selectedTheme,	// but theme isnt applied to jqchart because its SVG
		width: 720,
		height: 500,
		resizable: false,
		initContent: function () {
			//                        $('#searchTextButton').jqxButton({ width: '80px', disabled: true });

			// setup the chart
			//			console.log('initContent: jqxChart(chart_settings) ...');
			//			$('#jqxChart').jqxChart(chart_settings);
			//			console.log('initContent: jqxChart(chart_settings) ... DONE');
		},
		autoOpen: false
	});

	$("#printButton").jqxButton({theme: selectedTheme});
	$("#printButton").click(function () {
		var content = $('#jqxChart')[0].outerHTML;
		var newWindow = window.open('', '', 'width=800, height=500'),
		document = newWindow.document.open(),
		pageContent =
		'<!DOCTYPE html>' +
		'<html>' +
		'<head>' +
		'<link rel="stylesheet" href="http://www.jqwidgets.com/jquery-widgets-demo/jqwidgets/styles/jqx.base.css" type="text/css" />' +
		'<meta charset="utf-8" />' +
		'<title>jQWidgets Chart</title>' +
		'</head>' +
		'<body>' + content + '</body></html>';
		document.write(pageContent);
		document.close();
		newWindow.print();
	});

	$("#jpegButton").jqxButton({theme: selectedTheme});
	$("#jpegButton").click(function () {
		// call the export server to create a JPEG image
		$('#jqxChart').jqxChart('saveAsJPEG', 'myChart.jpeg', 'download.php');
	});

	$("#pngButton").jqxButton({theme: selectedTheme});
	$("#pngButton").click(function () {
		// call the export server to create a PNG image
		$('#jqxChart').jqxChart('saveAsPNG', 'myChart.png', 'download.php');
	});

});

//# sourceURL=tab_showallgrid.js