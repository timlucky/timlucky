<?php
//session_start();
    require_once('html_stream.php');
    require_once('string_formatter.php');

    class HTML_Parser {
      var $html_stream = '';
      var $doc = '';
      var $source_file = '';
      var $global_object = '';
      var $string_formatter = '';
      var $finished = '';

       function __construct($pSourceFile) {
           //To TEST --> GLOBAL OBJECTS
           //$this->init_global_object();
           $this->init_string_formatter();
           $this->init_html_stream();
           $this->init_doc();
           if ($pSourceFile != '') {
				$this->set_source_file($pSourceFile);
           }
           $this->set_finished(false);
       }

       function init_doc() {
           $this->doc = new DOMDocument();
       }

       function init_html_stream() {
            $this->html_stream = new HTML_Stream();
       }
       /*
       function init_global_object() {
           error_log('INITILIZE GLOBAL OBJECT...');
            $this->global_object = new GLOBAL_Object();
       }
       */

       function init_string_formatter() {
           $this->string_formatter = new STRING_Formatter();
       }

       function get_doc() {
           return $this->doc;
       }

        function set_source_file($pSourceFile) {
            $is_error = false;
            libxml_use_internal_errors(true);
            $this->source_file = $pSourceFile;
            $this->get_doc()->loadHTMLFile($this->get_source_file());
            $errors = libxml_get_errors();
                for($i = 0; $i<count($errors); $i++){
                    foreach ($errors[$i] as $error) {
                        if(strpos($error, 'input conversion failed due to input error') !== false) $is_error = true;
                }
            }
            if ($is_error) {
                error_log('INVALID DOCUMENT "' . $pSourceFile . '"! UNLOAD SOURCE FILE...');
                echo 'INVALID DOCUMENT "' . $pSourceFile . '"! UNLOAD SOURCE FILE... <br>';
                //$this->source_file = '';
                echo $this->source_file . ' <br>';
                error_log('DOCUMENT UNLOADED!');
                echo 'DOCUMENT UNLOADED! <br>';
            }
            libxml_clear_errors();
        }

       function get_source_file() {
           return $this->source_file;
       }

       function get_html_stream() {
           return $this->html_stream;
       }

       function get_string_formatter() {
           return $this->string_formatter;
       }

       function set_finished($pFinished) {
           $this->finished = $pFinished;
       }

       function get_finished() {
           return $this->finished;
       }
       /*
       function get_global_object() {
           return $this->global_object;
       }
       */
  }
?>
