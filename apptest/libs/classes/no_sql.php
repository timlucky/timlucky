<?php
    class No_SQL {
        var $host = '';
        var $db = '';
        var $collection = '';
        var $no_sql_instance = '';

        function __construct(){
            $this->set_host('localhost');
            $this->init_no_sql();
            $this->select_db('spyder');
            $this->set_collection('imdb_datas');
        }

        function init_no_sql(){
            $tmp_host = $this->get_host();
            $this->no_sql_instance = new Mongo("mongodb://$tmp_host");
        }

        function select_db($pDb){
            $this->db = $this->get_no_sql_instance()->selectDB($pDb);
        }

        function no_sql_find(){
            $find = '';
            $find = $this->get_collection()->find();
            return $find;
        }

        function no_sql_find_one($pConditions, $pSelect){
            return $this->get_collection()->findOne($pConditions, $pSelect);
        }

        function get_db(){
            return $this->db;
        }

        function get_host(){
            return $this->host;
        }

        function get_collection(){
            return $this->collection;
        }

        function get_no_sql_instance(){
            return $this->no_sql_instance;
        }

        function set_db($pDb){
            $this->db = $this->get_no_sql_instance()->selectDB($pDb);
        }

        function set_collection($pCollection){
            $this->collection = $this->get_db()->$pCollection;
        }

        function set_host($pHost){
            $this->host = $pHost;
        }

        function insert($pInsert){
            $this->get_collection()->insert($pInsert);
        }

        function update($pWhere, $pUpdate){
            $this->get_collection()->update($pWhere, $pUpdate);
        }

        function remove($pWhere){
            $this->get_collection()->remove($pWhere);
        }

        function create_collection($pCollection){
            $this->get_db()->createCollection($pCollection,false);
        }
    }
?>
