<?php
class STRING_Formatter {
    var $string = '';
      
    function __construct() {
          
    }
    
    function has_to_skip($pChar, $pSkipArray) {
        if (in_array($pChar, $pSkipArray)) return true;
        return false;       
    }
      
    function clean_string($pString, $pReplaceSpaces, $pSkipArray) {
        array_push($pSkipArray, $pReplaceSpaces);
        $this->set_string($pString);
        $tmp_string = $this->get_string();
        $tmp_string = strtolower($tmp_string);
        $tmp_string = str_replace('Ä', 'ä', $tmp_string);
        $tmp_string = str_replace('Ö', 'ö', $tmp_string);
        $tmp_string = str_replace('Ü', 'ü', $tmp_string);
        
        $tmp_string = str_replace(' ', $pReplaceSpaces, $tmp_string);
        
        if (!$this->has_to_skip('^', $pSkipArray)) $tmp_string = str_replace('^', '', $tmp_string); 
        if (!$this->has_to_skip('á', $pSkipArray)) $tmp_string = str_replace('á', 'a', $tmp_string);
        if (!$this->has_to_skip('à', $pSkipArray)) $tmp_string = str_replace('à', 'a', $tmp_string);
        if (!$this->has_to_skip('â', $pSkipArray)) $tmp_string = str_replace('â', 'a', $tmp_string);
        if (!$this->has_to_skip('ó', $pSkipArray)) $tmp_string = str_replace('ó', 'o', $tmp_string);
        if (!$this->has_to_skip('ò', $pSkipArray)) $tmp_string = str_replace('ò', 'o', $tmp_string);
        if (!$this->has_to_skip('ô', $pSkipArray)) $tmp_string = str_replace('ô', 'o', $tmp_string);
        if (!$this->has_to_skip('ú', $pSkipArray)) $tmp_string = str_replace('ú', 'u', $tmp_string);
        if (!$this->has_to_skip('ù', $pSkipArray)) $tmp_string = str_replace('ù', 'u', $tmp_string);
        if (!$this->has_to_skip('û', $pSkipArray)) $tmp_string = str_replace('û', 'u', $tmp_string);                                
        if (!$this->has_to_skip('é', $pSkipArray)) $tmp_string = str_replace('é', 'e', $tmp_string);
        if (!$this->has_to_skip('è', $pSkipArray)) $tmp_string = str_replace('è', 'e', $tmp_string);
        if (!$this->has_to_skip('ê', $pSkipArray)) $tmp_string = str_replace('ê', 'e', $tmp_string);
        if (!$this->has_to_skip('í', $pSkipArray)) $tmp_string = str_replace('í', 'i', $tmp_string);
        if (!$this->has_to_skip('ì', $pSkipArray)) $tmp_string = str_replace('ì', 'i', $tmp_string);
        if (!$this->has_to_skip('î', $pSkipArray)) $tmp_string = str_replace('î', 'i', $tmp_string);
        if (!$this->has_to_skip('ß', $pSkipArray)) $tmp_string = str_replace('ß', 'ss', $tmp_string);
        if (!$this->has_to_skip('´', $pSkipArray)) $tmp_string = str_replace('´', '_', $tmp_string);
        
        if (!$this->has_to_skip('°', $pSkipArray)) $tmp_string = str_replace('°', '', $tmp_string);
        if (!$this->has_to_skip('!', $pSkipArray)) $tmp_string = str_replace('!', '_', $tmp_string);
        if (!$this->has_to_skip('"', $pSkipArray)) $tmp_string = str_replace('"', '_', $tmp_string);
        if (!$this->has_to_skip('§', $pSkipArray)) $tmp_string = str_replace('§', '_', $tmp_string);
        if (!$this->has_to_skip('$', $pSkipArray)) $tmp_string = str_replace('$', '_', $tmp_string);
        if (!$this->has_to_skip('%', $pSkipArray)) $tmp_string = str_replace('%', '_', $tmp_string);
        if (!$this->has_to_skip('&', $pSkipArray)) $tmp_string = str_replace('&', '_', $tmp_string);
        if (!$this->has_to_skip('/', $pSkipArray)) $tmp_string = str_replace('/', '_', $tmp_string);
        if (!$this->has_to_skip('(', $pSkipArray)) $tmp_string = str_replace('(', '_', $tmp_string);
        if (!$this->has_to_skip(')', $pSkipArray)) $tmp_string = str_replace(')', '_', $tmp_string);
        if (!$this->has_to_skip('=', $pSkipArray)) $tmp_string = str_replace('=', '_', $tmp_string);
        if (!$this->has_to_skip('?', $pSkipArray)) $tmp_string = str_replace('?', '_', $tmp_string);
        if (!$this->has_to_skip('`', $pSkipArray)) $tmp_string = str_replace('`', '_', $tmp_string);
        
        if (!$this->has_to_skip('²', $pSkipArray)) $tmp_string = str_replace('²', '_', $tmp_string);
        if (!$this->has_to_skip('³', $pSkipArray)) $tmp_string = str_replace('³', '_', $tmp_string);
        if (!$this->has_to_skip('{', $pSkipArray)) $tmp_string = str_replace('{', '_', $tmp_string);
        if (!$this->has_to_skip('[', $pSkipArray)) $tmp_string = str_replace('[', '_', $tmp_string);
        if (!$this->has_to_skip(']', $pSkipArray)) $tmp_string = str_replace(']', '_', $tmp_string);
        if (!$this->has_to_skip('}', $pSkipArray)) $tmp_string = str_replace('}', '_', $tmp_string);
        if (!$this->has_to_skip('\\', $pSkipArray)) $tmp_string = str_replace('\\', '_', $tmp_string);
        
        if (!$this->has_to_skip('ä', $pSkipArray)) $tmp_string = str_replace('ä', 'ae', $tmp_string);
        if (!$this->has_to_skip('ö', $pSkipArray)) $tmp_string = str_replace('ö', 'oe', $tmp_string);
        if (!$this->has_to_skip('ü', $pSkipArray)) $tmp_string = str_replace('ü', 'ue', $tmp_string);
        if (!$this->has_to_skip('+', $pSkipArray))$tmp_string = str_replace('+', '_', $tmp_string);
        if (!$this->has_to_skip('#', $pSkipArray)) $tmp_string = str_replace('#', '_', $tmp_string);
        if (!$this->has_to_skip('<', $pSkipArray)) $tmp_string = str_replace('<', '', $tmp_string);
        if (!$this->has_to_skip(',', $pSkipArray)) $tmp_string = str_replace(',', '', $tmp_string);
        if (!$this->has_to_skip('.', $pSkipArray)) $tmp_string = str_replace('.', '', $tmp_string);
        if (!$this->has_to_skip('-', $pSkipArray)) $tmp_string = str_replace('-', '', $tmp_string);
        
        if (!$this->has_to_skip('*', $pSkipArray)) $tmp_string = str_replace('*', '_', $tmp_string);
        if (!$this->has_to_skip('\'', $pSkipArray)) $tmp_string = str_replace('\'', '', $tmp_string);
        if (!$this->has_to_skip('>', $pSkipArray)) $tmp_string = str_replace('>', '', $tmp_string);
        if (!$this->has_to_skip(';', $pSkipArray)) $tmp_string = str_replace(';', '', $tmp_string);
        if (!$this->has_to_skip(':', $pSkipArray)) $tmp_string = str_replace(':', '', $tmp_string);
        if (!$this->has_to_skip('_', $pSkipArray)) $tmp_string = str_replace('_', '', $tmp_string);
        
        if (!$this->has_to_skip('@', $pSkipArray)) $tmp_string = str_replace('@', '', $tmp_string);
        if (!$this->has_to_skip('€', $pSkipArray)) $tmp_string = str_replace('€', '', $tmp_string);
        if (!$this->has_to_skip('~', $pSkipArray)) $tmp_string = str_replace('~', '_', $tmp_string);
        if (!$this->has_to_skip('|', $pSkipArray)) $tmp_string = str_replace('|', '', $tmp_string);
        if (!$this->has_to_skip('’', $pSkipArray)) $tmp_string = str_replace('’', '', $tmp_string);
        if (!$this->has_to_skip('–', $pSkipArray)) $tmp_string = str_replace('–', '', $tmp_string);
        //if (!$this->has_to_skip('^', $pSkipArray)) $tmp_string = trim($tmp_string);
        $this->set_string($tmp_string);  
        return $this->get_string();
      }
      
      function set_string($pString) {
          $this->string = $pString;
      }
      
      function get_string() {
          return $this->string;
      }
      //overwrite Functions
      function __toString(){
          return $this->get_string();
      }
  }
?>
