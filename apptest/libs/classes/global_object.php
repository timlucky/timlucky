<?php
session_start();
require_once('libs/classes/string_formatter.php');

class GLOBAL_Object {
    var $string_formatter = '';
    
    function __construct() {
        $this->init_string_formatter();    
    }
    
    function init_string_formatter() {
        error_log('INNITILIZE STRING FORMATTER...');
        if (!isset($_SESSION['STRING_formatter'])) {
             error_log('INITILIZE NEW INSTANCE OF -->STRING_Formatter<-- ! <br>');
            $this->string_formatter = new STRING_Formatter();
            $_SESSION['STRING_formatter'] = $this->string_formatter;
            
        } else {
            error_log('FOUND EXISTING INSTANCE OF -->STRING_Formatter<-- ! <br>');
        }
    }
    
    function set_string_formatter($pStringFormatter) {
        error_log('SETTING GLOBAL INSTANCE OF -->STRING FORMATTER<--');
        $this->string_formatter = $pStringFormatter;
    }
    
    function get_string_formatter() {
        error_log('RESPONSE FOR -->GET_STRING_FORMATTER<--');
        return $_SESSION['STRING_formatter'];
    }
}
?>
