<?php
//error_reporting(0);
error_reporting(0);
require_once('libs/classes/html_parser.php');
require_once('libs/classes/no_sql.php');

class FILMstarts_parser extends HTML_Parser{
	function __construct($pSourceFile){
		parent::__construct($pSourceFile);
	}

	function parse_html($pSourceFile){
		$this->get_doc()->loadHTMLFile($pSourceFile);
		$elements = $this->get_doc()->getElementsByTagName('div');
		$here = false;
		foreach ($elements as $element) {
			if ($element->getAttribute('class') == 'vmargin10t') {
				$nodes = $element->childNodes;
				foreach ($nodes as $node) {
					if (strpos($node->nodeName, 'div') !== false) {
						if ($node->getAttribute('class') == 'titlebar') {
							$child_nodes = $node->childNodes;
							foreach ($child_nodes as $child_node) {
								if (strpos($child_node->nodeName, 'h2') !== false) {
									if (trim($child_node->nodeValue) == 'Filme') {
										$here = true;
									}
								}
							}
						}
					}

					if ($here) {
						if (trim($node->nodeName) == 'table') {
							$tbodys = $node->childNodes;
							foreach ($tbodys as $tbody) {
								$trs = $tbody->childNodes;
								foreach ($trs as $tr) {
									$tds = $tr->childNodes;
									foreach ($tds as $td) {
										$td_elements =  $td->childNodes;
										foreach ($td_elements as $td_element) {
											if (strpos($td_element->nodeName, 'a') !== false) {
												$href = $td_element->getAttribute('href');
												//error_log('$href --> ' . $href);
											}
										}
									}
								}
							}
							$here = false;
						}
						//$here = false;
					}
				}
			}
		}
		//return true;
	}

	function store_filmstarts_data(){
		$url = '';
		$m_no_sql = new No_SQL();
		$conditions = array('href' => '/title/tt1170358/');
	    $select = array('title');
		$return = $m_no_sql->no_sql_find_one($conditions, $select);
	    $result_array = array();
	    if ($return){
	    	$url = 'http://www.filmstarts.de/suche/?q=' . $return['title'];
	    	$o = '';
	    	$e = '';
	    	//exec('phantomjs savepage.js "' . $url . '" "data/html_sources/filmstarts_warner/filmstarts_warner.html"', $o, $e);
	    	$this->parse_html('data/html_sources/filmstarts_warner/filmstarts_warner.html');
	    	/*
			foreach($return as $n_key) {
    			if ($n_key == 'title' || true) {
					array_push($result_array, $val);
					$url = 'http://www.filmstarts.de/suche/?q=' . $return['title'];
					//error_log('$url --> ' . $n_key);
    			}
	    	}
	    	*/
	    }
		//return $url;
	}
}
?>
