<?php
//error_reporting(0);
require_once('libs/classes/html_parser.php');
require_once('libs/classes/no_sql.php');
class Mediabiz_parser extends HTML_Parser {
    var $output_path = '';
    function __construct($pSourceFile) {
            parent::__construct($pSourceFile);
       }

    function generate_url(){
		$result_array = array();
		$no_sql = new No_SQL();
		$cursor = $no_sql->no_sql_find();
	    foreach($cursor as $n_key) {
	    	array_push($result_array, $n_key);
	    }
	    return $result_array;
    }

       function get_error_pages($pPath, $pPageCount=0) {
           $break = false;
           $file_name= '';
           $path_array = array();
           foreach (glob($pPath . '*.html') as $filename) {
               if ($break) break;
               $pPageCount++;
                $this->get_doc()->loadHTMLFile($filename);
                $elements = $this->get_doc()->getElementsByTagName('p');
                foreach ($elements as $element) {
                    if (strpos($element->getAttribute('class'), 'search_string') !== false) {
                        $path_array = explode('/', $filename);
                        $last = count($path_array) - 1;
                        $file_name = $path_array[$last];
                        $file_name = str_replace('.html', '', $file_name);
                        $file_name = trim($file_name);
                        unset($path_array[$last]);
                        if (strpos($element->nodeValue, 'ergab leider keine Treffer.') !== false) {
                           $new = implode('/', $path_array) . '/error_pages/' . $file_name . '.html';

                        } else {
                            $new = implode('/', $path_array) . '/working_pages/' . $file_name . '.html';
                        }
                        rename($filename, $new);
                        $break = true;
                    }
                }
           }
           if ($break) echo 'SCANNED FILE--> ' . $file_name . '.html as NUMBER--> ' . $pPageCount . ' <script>ajax_get(\'page_count=' . $pPageCount . '\');</script>';
           else echo 'FINISHED <br>';
       }

       function print_mediabiz_html_to_file($pUrl, $pTargetFileName, $pType) {
            $this->get_html_stream()->get_content_url($pUrl);
            echo $this->get_html_stream()->get_content() . ' <br>';
            //$this->get_html_stream()->print_to_file('temp/mediabiz/' . $pType . '/from_imdb/' . $pTargetFileName);
       }

       function set_output_path($pOutputPath) {
           $this->output_path = $pOutputPath;
       }

       function get_output_path() {
            return $this->output_path;
       }
}
?>
