<?php
//error_reporting(0);
require_once('libs/classes/html_parser.php');

class Fernseh_Serien_Parser extends HTML_Parser {
      
      function __construct($pSourceFile) {
          parent::__construct($pSourceFile);
      }
      
      function parse_title() {
            $this->get_doc()->loadHTMLFile($this->get_source_file());
            $elements = $this->get_doc()->getElementsByTagName('li');
            $one = 0;
            $two = 0;
            $first_year = '';
            $first_bracket_pos = 0;
            $last_sign = '';
            $file_name = '';
            $title = '';
            $theatrical_url = '';
            $physical_url = '';
            $output = '';
            $output_path = '';

            foreach ($elements as $element) {
                $nodes = $element->childNodes;
                foreach ($nodes as $node) {
                    if (strpos($node->nodeName, 'a') !== false ) {
                        $title = $node->getAttribute('title');
                        $one = strpos($title, '1');
                        $two = strpos($title, '2');
                        if ($one !== false || $two !== false) {
                            $first_year_number_pos = $one;
                            if (($one > $two) && $two !== false) $first_year_number_pos = $two;
                            $first_year = substr($title, $first_year_number_pos, 4);
                            if ($first_year >= 1990) {
                                $first_bracket_pos = strpos($title, '(');
                                if ($first_bracket_pos !== false) $title = substr($title, 0, $first_bracket_pos);
                                $file_name = $this->get_string_formatter()->clean_string($title, '_', array());
                                $title = $this->get_string_formatter()->clean_string($title, '+', array());
                                
                                $last_sign = substr($title, strlen($title)-1, strlen($title));
                                if ($last_sign == '+') $title = substr($title, 0, -1);
                                
                                $last_sign = substr($title, strlen($title)-1, strlen($title));
                                if ($last_sign == '_') $title = substr($title, 0, -1);
                                
                                $last_sign = substr($file_name, strlen($file_name)-1, strlen($file_name));
                                if ($last_sign == '_') $file_name = substr($file_name, 0, -1); 
                                $file_name .= '.html';
                                
                                $theatrical_url = 'http://www.mediabiz.de/suche/result?searchCategories=kinofilm&t=';
                                $physical_url = 'http://www.mediabiz.de/suche/result?searchCategories=videofilm&t=';
                                
                                $theatrical_url .= $title;
                                $output_path = 'D:\Xamp\htdocs\nt_webapp\htdocs\temp\mediabiz\theatrical\serien\from_f_s_de';
                                $output = '<p id=\'request_url\'>' . $theatrical_url . '</p> <br>';
                                $output .= '<br><p id=\'output_path\'>' . $output_path . '</p> <br> ';
                                $output .= '<br><p id=\'file_name\'>' . $file_name . '</p> <br> ';
                                file_put_contents('temp/mediabiz/theatrical/serien/from_f_s_de/request_queue/request_' . $file_name, $output);
                                
                                $physical_url .= $title;
                                $output_path = 'D:\Xamp\htdocs\nt_webapp\htdocs\temp\mediabiz\physical\serien\from_f_s_de';
                                $output = '<p id=\'request_url\'>' . $physical_url . '</p> <br>';
                                $output .= '<br><p id=\'output_path\'>' . $output_path . '</p> <br> ';
                                $output .= '<br><p id=\'file_name\'>' . $file_name . '</p> <br> ';
                                //file_put_contents('temp/mediabiz/physical/serien/from_f_s_de/request_queue/request_' . $file_name, $output);
                            }
                        }
                }   
            }        
      }
      //D:\Xamp\htdocs\nt_webapp\htdocs\temp\mediabiz\theatrical\serien\from_f_s_de
  }
  
  function get_mediabiz_request($pPath) {
    $this->set_finished(false);
    $break = false;
    $tnp_count = 0;
    foreach (glob($pPath . '*.html') as $filename) {
        if ($break) break;
       $tnp_count++;
       $path_array = explode('/', $filename);
       $last = count($path_array) - 1;
       $file_name = $path_array[$last];
       $file_name = str_replace('.html', '', $file_name);
       $file_name = trim($file_name);
       unset($path_array[$last]);
       echo file_get_contents($filename);
       $new = implode('/', $path_array) . '/scanned/' . $file_name . '.html';
       rename($filename, $new);
       if ($tnp_count >= 1) $break = true;
    }
    if (!$break) {
        $this->set_finished(true);
        $output = '<p id=\'request_url\'>http://127.0.0.1:8010/ajax.php</p> <br>';
        $output .= '<br><p id=\'output_path\'>D:\Xamp\htdocs\nt_webapp\htdocs\temp</p> <br> ';
        $output .= '<br><p id=\'file_name\'>no_request.html</p> <br> ';
        //echo $output;        
    } 
  }
}

?>
