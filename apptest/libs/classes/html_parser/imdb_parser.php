<?php
error_reporting(0);
require_once('libs/classes/html_parser.php');
require_once('libs/classes/html_parser/mediabiz_parser.php');
require_once('libs/classes/no_sql.php');

class IMDB_parser extends HTML_Parser {
    var $output_path = '';
    var $mediabiz_parser = '';
    function __construct($pSourceFile, $pOutputPath) {
            parent::__construct($pSourceFile);
            $this->set_output_path($pOutputPath);
            //$this->init_mediabiz_parser();
       }

    function parse_html_to_file($pStart=0, $pMax=1) {
        $this->get_doc()->loadHTMLFile($this->get_source_file());
        $break = false;
        $count = 0;
        $elements = $this->get_doc()->getElementsByTagName('ol');
        if (!is_null($elements)) {
            foreach ($elements as $element) {

            }
       }
    }

    function get_datas($pSourceFile) {
        $this->get_doc()->loadHTMLFile($pSourceFile);
        $elements = $this->get_doc()->getElementsByTagName('ol');
        foreach ($elements as $element) {
            $nodes = $element->childNodes;
            foreach ($nodes as $node) {
                $child_nodes = $node->childNodes;
                foreach ($child_nodes as $child_node) {
                    $out = '';
                    //get Title
                    if (strpos('a', $child_node->nodeName) !== false ) {
                        $href = $child_node->getAttribute('href');
                        $title = $child_node->nodeValue;
                        $out .=$href . ' ' . $title . ' ';
                    }
                    //get Release Date
                    if (strpos($child_node->nodeName, '#text' ) !== false ) {
                        $release_date = $child_node->nodeValue;
                        $release_date = substr($release_date, 2, 4);
                        $out .= $release_date . ' ';
                        if (strpos($release_date, '?') === false ){

                        }
                    }
                }
            }
        }
    }


    function get_release_date($pSourceFile) {
       $this->get_doc()->loadHTMLFile($pSourceFile);
        $elements = $this->get_doc()->getElementsByTagName('ol');
        foreach ($elements as $element) {
            $nodes = $element->childNodes;
            foreach ($nodes as $node) {
                $child_nodes = $node->childNodes;
                foreach ($child_nodes as $child_node) {
                    if (strpos($child_node->nodeName, '#text' ) !== false ) {
                        $release_date = $child_node->nodeValue;
                        $release_date = substr($release_date, 2, 4);
                        if (strpos($release_date, '?') === false ){

                        }
                    }
                }
            }
        }
    }

       function parse_files($pPath, $pExecutionType = '') {
           $path_array = array();
           $last = 0;
           $file_name = '';
           $new = '';
           $theatrical_url = '';
           $physical_url = '';
           $title = '';
           $orig_title = '';
           $break = false;
           if ($pExecutionType == 'sort_files') {
                foreach (glob($pPath . '*.html') as $filename) {
                 if ($break) break;
                 $path_array = explode('/', $filename);
                 $last = count($path_array) - 1;
                 $file_name = $path_array[$last];
                 unset($path_array[$last]);
                 if ($this->get_release_date($filename) < 1990) {
                     $new = implode('/', $path_array) . '/archive/' . $file_name;
                     rename($filename, $new);
                 } else {
                     $new = implode('/', $path_array) . '/actual/' . $file_name;
                     rename($filename, $new);
                 }
                 $break = true;
            }
            if ($break) echo 'FILE --> ' . $file_name . ' MOVED TO --> ' . $new . ' <script>ajax_get(\'1\');</script>';
            else 'FINISHED!';
            }

           if ($pExecutionType == 'get_mediabiz_overview_pages') {
               //$url = 'http://www.mediabiz.de/suche/result?searchCategories=kinofilm&t=Der+gro%DFe+Gatsby';
               //$url = 'http://www.mediabiz.de/suche/result?searchCategories=kinofilm&t=Der+große+Gatsby';
               $tnp_count = 0;
               foreach (glob($pPath . '*.html') as $filename) {
                   $tnp_count++;
                   $path_array = explode('/', $filename);
                   $last = count($path_array) - 1;
                   $file_name = $path_array[$last];
                   $file_name = str_replace('.html', '', $file_name);
                   $file_name = trim($file_name);
                   unset($path_array[$last]);
                   if ($break) break;
                   $theatrical_url = 'http://www.mediabiz.de/suche/result?searchCategories=kinofilm&t=';
                   $physical_url = 'http://www.mediabiz.de/suche/result?searchCategories=videofilm&t=';
                   $title = $this->get_title($filename);
                   $orig_title = $title;
                   $title = str_replace(' ', '+', $title);
                   $theatrical_url .= $title;
                   $physical_url .= $title;

                   //exec('phantomjs savepage.js "http://www.mediabiz.de/suche/result?searchCategories=kinofilm&t=The+Hobbit&selectedAreaByResult="', $o, $e);
                   $dbhost = 'localhost';
                   $dbname = 'test';
                   $collection = 'my_collection';
                   $no_sql = new No_SQL($dbhost, $dbname, $collection);
                   $file = '';
                   $file = '1.html';
                   $doc = '';
                   $doc = file_get_contents($file);
                   $insert = array("title" => $doc);
                   $no_sql->insert($insert);
                   unlink($file);

                   echo  '<br><p id=\'request_url\'>' . $physical_url . '</p> <br>';
                   echo  '<br><p id=\'file_name\'>' . $file_name . '</p> <br>';
                   //echo  'PHYSICAL REQUEST --> <a target=\'blank\' href=\'' . $physical_url . '\'>' . $orig_title . ' </a> <br>';
                   //$this->get_mediabiz_parser()->print_mediabiz_html_to_file($theatrical_url, $file_name, 'theatrical');
                   //$this->get_mediabiz_parser()->print_mediabiz_html_to_file($physical_url, $file_name, 'physical');
                   $new = implode('/', $path_array) . '/scanned/' . $file_name . '.html';
                   rename($filename, $new);
                   /*
                     $path_array = explode('/', $filename);
                     $last = count($path_array) - 1;
                     $file_name = $path_array[$last];
                     unset($path_array[$last]);
                     $new = implode('/', $path_array) . '/archive/' . $file_name;
                     rename($filename, $new);
                     */
                   if ($tnp_count >= 1) $break = true;
            }
            //if ($break) echo 'FILE --> ' . $file_name . ' MOVED TO --> ' . $new . ' <script>ajax_get(\'1\');</script>';
            //else 'FINISHED!';
           }
       }

       function set_output_path($pOutputPath) {
           $this->output_path = $pOutputPath;
       }

       function get_output_path() {
            return $this->output_path;
       }

       function init_mediabiz_parser() {
           $this->mediabiz_parser = new Mediabiz_parser($this->get_source_file());
       }

       function get_mediabiz_parser() {
            return $this->mediabiz_parser;
       }

    function store_imdb_datas($pSourceFile){
    	$day = date('d');
    	$month = date('m');
    	$year = date('Y');
    	$today = $year . '_' . $month . '_' . $day;
		$day -= 1;
		$yesterday = $year . '_' . $month . '_' . $day;

    	if (!file_exists('data/html_sources/imdb_warner/imdb_warner_' . $today . '.html')) {
    		error_log('Scanning IMDB...');
			exec('phantomjs savepage.js "http://www.imdb.com/company/co0026840/?ref_=fn_co_co_2" "data/html_sources/imdb_warner/imdb_warner_' . $today . '.html"', $o, $e);
			error_log('Scanning IMDB successsfully');
			error_log('Deleting old Source File from: ' . $yesterday);
			@unlink('data/html_sources/imdb_warner/imdb_warner_' . $yesterday . '.html');
			$pSourceFile = 'data/html_sources/imdb_warner/imdb_warner_' . $today . '.html';
	        $no_sql = new No_SQL();
	        $no_sql->create_collection('imdb_datas');
	        $no_sql->set_collection('imdb_datas');
	        $this->get_doc()->loadHTMLFile($pSourceFile);
	        $elements = $this->get_doc()->getElementsByTagName('ol');
	        $insert_count = 0;
	        $update_count = 0;
	        foreach ($elements as $element) {
	            $nodes = $element->childNodes;
	            foreach ($nodes as $node) {
	                $child_nodes = $node->childNodes;
	                if (count($child_nodes) > 0) {
						foreach ($child_nodes as $child_node) {
	                    //get Title
		                    if (strpos('a', $child_node->nodeName) !== false ) {
		                        $href = $child_node->getAttribute('href');
		                        $title = $child_node->nodeValue;
		                        $title = utf8_decode($title);
		                    }
		                    //get Release Date
		                    if (strpos($child_node->nodeName, '#text' ) !== false ) {
		                        $release_date = $child_node->nodeValue;
		                        $release_date = substr($release_date, 2, 4);
		                        if (strpos($release_date, '?') !== false || $release_date >= 1990){
		                            $return = false;
		                            $conditions = array('title' => $title);
		                            $select = array('id');
		                            $return = $no_sql->no_sql_find_one($conditions, $select);
		                            if ($return) {
	                            		$update_count++;
		                                $update = array('$set' => array('href' => $href, 'release_date' => $release_date, 'last_update' => $today));
		                                $where = array('title' => $title);
		                                $no_sql->update($where, $update);
		                            } else{
	                            		$insert_count++;
		                                $insert = array('title' => $title, 'href' => $href, 'release_date' => $release_date, 'last_update' => $today, 'scanned_filmstarts' => 0, 'scanned_mediabiz' => 0, 'scanned_boxoffice_mojo' => 0, 'scanned_itunes_store' => 0, 'scanned_boxoffice_amazon' => 0, 'scanned_boxoffice_ebay' => 0);
		                                $no_sql->insert($insert);
		                            }
		                        } else {

		                        }
		                    }
		               }
	                }
	            }
	        }
    	} else {
			error_log('IMDB already scanned!');
    	}
    }

    function parse_charts($pHref){
		$url = 'http://www.imdb.com' . $pHref;
		exec('phantomjs savepage.js "http://www.imdb.com/company/co0026840/?ref_=fn_co_co_2" "data/html_sources/imdb_warner/imdb_warner_charts_tmp.html"', $o, $e);
		$this->get_doc()->loadHTMLFile('data/html_sources/imdb_warner/imdb_warner_charts_tmp.html');
	    $elements = $this->get_doc()->getElementsById('tn15adrhs');
		unlink('data/html_sources/imdb_warner/imdb_warner_charts_tmp.html');
    }
}

?>
