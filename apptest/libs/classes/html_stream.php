<?php
class HTML_Stream {
	var $opts = '';
    var $context = '';
	var $content = '';
	var $url = '';
	
	function __construct(){
		$this->set_opts(array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n" 
		  )
		));

		$this->set_context(stream_context_create($this->get_opts()));
	}
	
	function print_to_file($pPath){
		file_put_contents($pPath, $this->content);
	}
	
	function get_content_url($pUrl){
		$this->url = $pUrl;
		$this->content = file_get_contents($this->url, false, $this->context);
	}
    
    function set_opts($pOtps) {
         $this->opts = $pOtps;
    }
    
    function set_context($pContext) {
         $this->context = $pContext;
    }
    
    function set_content($pContent) {
         $this->content = $pContent;
    }
    
    function set_url($pUrl) {
         $this->url = $pUrl;
    }
    
    function get_opts() {
        return $this->opts;
    }
    
    function get_context() {
        return $this->context;
    }
    
    function get_content() {
        return $this->content;
    }
    
    function get_url() {
        return $this->url;
    }
}
?>