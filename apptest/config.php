<?php
// global config
global $_cfg;
$_cfg = array();
$_cfg['database_name'] = 'spyder';
$_cfg['collection_movies'] = 'movies';
$_cfg['collection_progress'] = 'progress';
$_cfg['collection_youtube'] = 'youtube';
$_cfg['collection_dynlists'] = 'dynlists';
$_cfg['collection_dynlists_prefix'] = 'dl_';
$_cfg['collection_genre'] = $_cfg['collection_dynlists_prefix'] . 'genre';


$_SESSION['_cfg'] = &$_cfg;	// so functions dont have to "global $_cfg;" ... simply use _cfg() function
?>
