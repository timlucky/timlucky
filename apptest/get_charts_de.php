<?php
/**
* main function
*
*/
function main($_id = 0) {
	$log = array();

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	$osr = array();
	$osi = array();
	$week_max = 0;
	$count = 0;
	$visitors_we_max = 0;
	$visitors_overall_max = 0;
	if ($_id > 0) {
		$sort = array('_id' => 1);	// order of entered in db
		$query = array('title' => 'Gravity');
		$query = array('_id' => $_id);
		$projection = array('_id' => 0, 'charts_germany' => 1);
		$cursor = safe_session('collection')->find($query, $projection)->sort($sort);
		while ($cursor->hasNext() ) {
			//			$os .= "<br>" . print_r($cursor->getNext(), true);
			$d = $cursor->getNext();
			if (isset($d['charts_germany'])) {
				if (count($d['charts_germany']['chart']) > 0) {	// may be empty ...
					foreach ($d['charts_germany']['chart'] as $i => $chart) {
						$count++;
						$date_from = $chart['date_from'];
						$visitors_we = $chart['visitors_we'];
						$visitors_we_max = MAX($visitors_we_max, $visitors_we);
						$visitors_overall = $chart['visitors_overall'];
						$visitors_overall_max = MAX($visitors_overall_max, $visitors_overall);
						$week_max = MAX($week_max, $chart['week']);
						$osr[] = array('date_from' => $date_from, 'visitors_we' => $visitors_we, 'visitors_overall' => $visitors_overall, 'week' => $chart['week']);
					}
				}
			}
		}
	}
	$osi = array('visitors_we_max' => $visitors_we_max, 'visitors_overall_max' => $visitors_overall_max, 'count' => $count);

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$log[] = "needs $time seconds";
	$after = memory_get_usage(true);
	$log[] = "memory used = " . (int)(($after - $before)/1024/1024 + 0.999) . " Mbytes";

	$oa = array('result' => ($count > 0) ? true : false, 'log' => $log, 'infos' => $osi, 'values' => $osr);

	return $oa;
}


////////
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_movies'));

$_id = (int)safe_request('_id', 0);
$r = main($_id);
echo json_encode($r);
?>
