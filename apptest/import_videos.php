<?php

/**
* here were importing all needed key/values into document (and possibly mapping then)
*
* @param mixed $doc
* @param mixed $each
* @param mixed $dynlists
*/
function views2doc(&$doc, &$each, &$dynlists) {
	/*
	<row>
	<field name="id">1</field>
	<field name="video_id">1</field>
	<field name="scan_date">2013-05-17 16:15:40</field>
	<field name="views">8112492</field>
	<field name="favourites">0</field>
	<field name="likes">25600</field>
	<field name="dislikes">1195</field>
	<field name="comments">8502</field>
	<field name="views_gain">8112492</field>
	<field name="likes_gain">25600</field>
	</row>
	*/
	set_int($doc, $each, 'video_id', 'id');
	$doc['scan'] = array();
	$doca = &$doc['scan'];
	set_string($doca, $each, 'scan_date', 'date');	// "yyyy-mm-dd hh:mm:ss"
	set_int($doca, $each, 'views');
	set_int($doca, $each, 'likes');
	set_int($doca, $each, 'dislikes');
	set_int($doca, $each, 'comments');
	//	set_int($doca, $each, 'views_gain');	// should be computable
	//	set_int($doca, $each, 'likes_gain');
}

/**
* callback function (reads xml and write to db)
*
*/
function import_videoviews($update = false) {
	$fnam = __FUNCTION__;
	$each = safe_session($fnam . '_param_each');

	// xml error handler
	libxml_use_internal_errors(true);

	//load xml
	$xml = simplexml_load_string(safe_session($fnam . '_param_xml'));

	// xml error handler
	if (!$xml) {
		$xmll = explode("\n", safe_session($fnam . '_param_xml'));
		$errors = libxml_get_errors();
		$es = '';
		foreach ($errors as $error) {
			$es .= display_xml_error($error, $xmll);
		}
		libxml_clear_errors();
		die(nl2br($es));
	}

	// get current dynlists
	$dynlists = &safe_session_r('dynlists', array());

	// global map list
	$_SESSION['map_values_list'] = array();

	// xml has structure:
	// <table_data name="video_views">
	//	<row>
	//		<field name="id">1</field>
	//		<field name="video_id">2</field>
	//		...
	//	</row>
	//	<row>
	//	...
	$array = 'field';
	$attr = 'name';

	$inserted = 0;
	$updated = 0;
	$ignored = 0;
	foreach ($xml->$each as $rowe) {
		// init empty doc
		$doc = array();

		// directly create arrayobject (same as xml-object for further converter)
		$row = new ArrayObject(array(), ArrayObject::ARRAY_AS_PROPS);
		foreach ($rowe->$array as $field) {
			$name = (string)$field[$attr];
			$value = (string)$field;
			$row->$name = $value;
		}

		// call reader
		views2doc($doc, $row, $dynlists);

		if (count($doc) > 0) {
			if (true) {
				// append array 'views' in document with matching video-id (not title-id) into that array with that id (MAGIC!!)
				$query = array('a' => array('$elemMatch' => array('id' => $doc['id'])));
				$update = array('$push' => array('a.$.scan' => $doc['scan']));
				$options = array('upsert' => true, 'multi' => false);
				try {
//					$r = safe_session('collection')->update($query, $update, $options);
					$r = safe_session('collection')->update($query, $update);
					$inserted++;
				} catch (MongoCursorException $e) {
					error_log("ignored video-id=" . $doc['id']);
//					error_log("query=" . print_r($query, true) . ", update=" . print_r($update, true));
					$ignored++;
				}
			} else {
				$ignored++;
				$query = array('a' => array('$elemMatch' => array('id' => $doc['id'])));
				$cursor = safe_session('collection')->find($query);
				while ($cursor->hasNext() ) {
					$d = $cursor->getNext();
				}

			}
		}
	}

	return array('inserted' => $inserted, 'updated' => $updated, 'ignored' => $ignored);
}


/**
* here were importing all needed key/values into document (and possibly mapping then)
*
* @param mixed $doc
* @param mixed $each
* @param mixed $dynlists
*/
function video2doc(&$doc, &$each, &$dynlists) {
	if ($each->deleted != '0') {	// if deleted then dont import ...
		return;
	}
	set_int($doc, $each, 'title_id', '_id');
	$doc['a'] = array();
	$doc['a'][0] = array();
	$doca = &$doc['a'][0];	// to have 1st insert 'a' as array (tricky!)
	set_int($doca, $each, 'id', 'id');
	set_string($doca, $each, 'video_title');
	set_int($doca, $each, 'duration');
	set_string($doca, $each, 'channel_name');
	set_string($doca, $each, 'upload_date');
	set_int($doca, $each, 'views');
	set_int($doca, $each, 'likes');
	set_int($doca, $each, 'dislikes');
	set_int($doca, $each, 'comments');
	set_string($doca, $each, 'platform_video_id', 'ytid');
	set_string($doca, $each, 'comment');
	set_string($doca, $each, 'created_at');
	set_string($doca, $each, 'last_scan');
	set_int($doca, $each, 'video_type');
	set_bool($doca, $each, 'official_video', '1');
	set_int($doca, $each, 'language');
	//	set_bool($doca, $each, 'deleted', '1');
	//	$doc['a'][0]['scan'] = array();	// empty for inserts on next stage (video_views)
}

/**
* callback function (reads xml and write to db)
*
*/
function import_videos($update = false) {
	$fnam = __FUNCTION__;
	$each = safe_session($fnam . '_param_each');

	// xml error handler
	libxml_use_internal_errors(true);

	//load xml
	$xml = simplexml_load_string(safe_session($fnam . '_param_xml'));

	// xml error handler
	if (!$xml) {
		$xmll = explode("\n", safe_session($fnam . '_param_xml'));
		$errors = libxml_get_errors();
		$es = '';
		foreach ($errors as $error) {
			$es .= display_xml_error($error, $xmll);
		}
		libxml_clear_errors();
		die(nl2br($es));
	}

	// get current dynlists
	$dynlists = &safe_session_r('dynlists', array());

	// global map list
	$_SESSION['map_values_list'] = array();

	// xml has structure:
	// <table_data name="videos">
	//	<row>
	//		<field name="id">1</field>
	//		<field name="video_title">After Earth Official Trailer #1 (2013) - Will Smith Movie HD</field>
	//		...
	//	</row>
	//	<row>
	//	...

	//	$o = array();
	//	$oi = 0;
	//	$array = 'field';
	//	$attr = 'name';
	//	foreach ($xml->$each as $row) {
	//		$o[$i] = array();
	//		foreach ($row->$array as $field) {
	//			$name = (string)$field[$attr];
	//			$value = (string)$field;
	//			$o[$i][$name] =  $value;
	//		}
	//		$i++;
	//	}

	$array = 'field';
	$attr = 'name';

	$inserted = 0;
	$updated = 0;
	$ignored = 0;
	foreach ($xml->$each as $rowe) {
		// init empty doc
		$doc = array();

		// put data in array and convert to arrayobject with properties ...
		//		$rowa = array();
		//		foreach ($rowe->$array as $field) {
		//			$name = (string)$field[$attr];
		//			$value = (string)$field;
		//			$rowa[$name] = $value;
		//		}
		//		$row = new ArrayObject($rowa, ArrayObject::ARRAY_AS_PROPS);

		// directly create arrayobject (same as xml-object for further converter)
		$row = new ArrayObject(array(), ArrayObject::ARRAY_AS_PROPS);
		foreach ($rowe->$array as $field) {
			$name = (string)$field[$attr];
			$value = (string)$field;
			$row->$name = $value;
		}

		// call reader
		video2doc($doc, $row, $dynlists);

		//error_log("document=" . print_r($doc, true));
		if (count($doc) > 0) {
			if (true) {
				$query = array('_id' => $doc['_id']);
				$options = array('upsert' => true, 'multi' => false);
				if ($update) {
					$r = safe_session('collection')->update($query, $doc, $options);
					if ($r['updatedExisting'] == false) {
						$inserted++;
					} else {
						$updated++;
					}
				} else {
					try {
						$r = safe_session('collection')->insert($doc);
						$inserted++;
					} catch(MongoCursorException $e) {
						// twice entered ... aborted ... updating
						$r = safe_session('collection')->update($query, array('$push' => array('a' => $doc['a'][0])), $options);
						if ($r['updatedExisting'] == false) {
							$inserted++;
						} else {
							$updated++;
						}
					}
				}
				//o($doc);
			}
		} else {
			$ignored++;
		}
	}

	return array('inserted' => $inserted, 'updated' => $updated, 'ignored' => $ignored);
}


//TODO: this might be in tools ...
/**
* handle xml function to read xml in parts and call callback
*
* @param mixed $callback
* @param mixed $file
* @param mixed $root
* @param mixed $each
* @param mixed $partsize
*
* @return array(inserted, updated)
*/
function handle_xml($callback, $file, $root, $each, $partsize = 1048576, $callback_params = array(), $progress_id = 1) {
	$first_close_s = '>';
	$root_s = '<' . $root . '>';
	$root_sl = strlen($root_s);
	$root_e = '</' . strtok($root, ' ') . '>';
	$each_s = '</' . $each . '>';
	$each_sl = strlen($each_s);

	$_SESSION[$callback . '_param_each'] = $each;

	// handle chunks
	if (!file_exists($file)) {
		return -1;
	}
	$rest = '';
	$header = '';

	$firstrun = true;
	$run = 0;
	$infos = array('inserted' => 0, 'updated' => 0, 'ignored' => 0);

	// set 0% to start js timer
	$filelength = filesize($file);
	$time_start = microtime(true);	// for progress indication
	setprogress($progress_id, -1, $infos);

	// open file and read chunks of given size
	$fp = fopen($file, 'rb');
	while ($read = fread($fp, $partsize)) {
		$run++;
		//		echo "run=$run<br>"; ob_flush(); flush();

		// fetch xml header and root (only on 1st run)
		$rp = 0;
		if ($firstrun) {
			$fcp = strpos($read, $first_close_s, 0);
			$header = substr($read, 0, $fcp + strlen($first_close_s));
			$rp = strpos($read, $root_s, 0);
			$rp += ($rp !== false) ? $root_sl : 0;
			$header .= $root_s;
			$firstrun = false;
		}

		// position of last $each in read chunk
		$last_each = strrpos($read, $each_s);
		if ($last_each === false) {	// none found ... fetch another chunk and append
			$rest .= $read;
			continue;
		}
		$last_each += ($last_each !== false) ? $each_sl : 0;

		// create valid xml
		$_SESSION[$callback . '_param_xml'] = $header . $rest . substr($read, $rp, $last_each - $rp) . $root_e;
		//error_log($_SESSION[$callback . '_param_xml']);
		// call callback (to import xml)
		$r = call_user_func_array($callback, $callback_params);
		$infos['inserted'] += $r['inserted'];
		$infos['updated'] += $r['updated'];
		$infos['ignored'] += $r['ignored'];

		// ending chunk bytes as leading for next chunk
		$rest = substr($read, $last_each);

		// progress indication
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		if ($time >= 5) {
			$percent = max(0, min(100, (int)(($partsize * $run) / $filelength * 100)));
			setprogress($progress_id, $percent, $infos);
			$time_start = $time_end;
		}
	}
	fclose($fp);

	// set 100% to stop js timer
	setprogress($progress_id, 100, $infos);

	// release memory used
	$rest = '';
	$_SESSION[$callback . '_param_xml'] = '';

	return $infos;
}



/**
* main function
*
* @param mixed $partialUpdate
*/
function main($partialUpdate = false, $progress_id = 2) {
	$os = '';

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	// init and read dynlists (possibly drop before also)
	$_SESSION['dynlists'] = array();
	//	read_dynlists($_SESSION['dynlists']);

	if (safe_request('t') == '1') {
		// read videos xml
		$file = './data/videos.xml';	// full base
		$os .= "<br>using file $file";
		safe_session('collection')->drop();
		$update = false;
		$root = 'table_data name="videos"';
		$each = 'row';
		$partsize = 1024*1024*1;	// 5M parts to fetch from input file
		$callback = 'import_videos';
		$r = handle_xml($callback, $file, $root, $each, $partsize, array($update), $progress_id);
		$os .= "<br>inserted " . $r['inserted'] . " + updated " . $r['updated'] . " + ignored " . $r['ignored'] . " youtube videos";
		safe_session('collection')->ensureIndex(array('a.id' => 1));	// speeds up step 2 with factor of 10!
		$os .= '<br>created index on a.id ... (needed for step 2 (t=2))';
	}

	if (safe_request('t') == '2') {
		$file = './data/video_views.xml';	// full base
		$os .= "<br>using file $file";
		$update = true;
		// read video_views xml
		$root = 'table_data name="video_views"';
		$each = 'row';
		$partsize = 1024*1024*1;	// 5M parts to fetch from input file
		$callback = 'import_videoviews';
		$r = handle_xml($callback, $file, $root, $each, $partsize, array($update), $progress_id);
		$os .= "<br>inserted " . $r['inserted'] . " + updated " . $r['updated'] . " + ignored " . $r['ignored'] . " youtube video_views";
	}

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	$os .= "<br>after importing xml in $time seconds";
	$time_start = $time_end;
	$after = memory_get_usage(true);
	$os .= " - memory used=($after - $before)=" . ($after - $before);
	$before = $after;

	// write back dynlists
	//	write_dynlists($_SESSION['dynlists']);

	$oa = array('result' => true, 'values' => $os);

	return $oa;
}



/////
// import_videos.php?t=1 ... to import video.xml (~6 seconds)
// import_videos.php?t=2 ... to import video_views.xml (~30 minutes)
////////
require_once('config.php');
require_once('tools.php');

open_database(_cfg('database_name'), _cfg('collection_youtube'));

//TODO: progress as in import (movies)
$progress_id = 2;	// should be in config

list($result, $v) = getprogress($progress_id);
if ($result) {
	if ($v != 0 && $v != 100) {	// seems to work ... abort
		die(json_encode(array('result' => false, 'info' => 'seems to still importing ...')));
	}
}

$partialUpdate = ((int)safe_request('use_xml_update', true) != 0) ? true : false;
$r = main($partialUpdate, $progress_id);

echo json_encode($r);
?>
