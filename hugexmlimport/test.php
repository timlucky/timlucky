<?php

//echo strtotime('1970-01-01 01:00:00');
//die();

/**
* safe get session variable (or default)
*
* @param mixed $k
* @param mixed $default
*/
function safe_session($k, $default = false) {
	if (isset($_SESSION[$k])) {
		return $_SESSION[$k];
	} else {
		return $default;
	}
}


/**
* output of html header
*
*/
function write_html_header() {
	$html_start = '<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
	<head>
	<meta charset="utf-8">
	<title>XML Test</title>
	</head>
	<body>
	';

	echo $html_start;
}


/**
* output of html footer
*
*/
function write_html_footer() {
	$html_end = '	</body>
	</html>';

	echo $html_end;
}



/**
* open and select a database
*
* @param mixed $dbname
* @param mixed $collectionname
*/
function open_database($dbname, $collectionname) {
	$username = '';
	$password = '';

	// connect
	$_SESSION['mongo'] = new MongoClient("mongodb://localhost");
	//$m = new MongoClient("mongodb://localhost", array("db" => $dbname, "username" => $username, "password" => $password));
	//$m = new Mongo("mongodb://127.0.0.1/comedy");
	// TODO: handle errors

	// select a database
	$_SESSION['db'] = $_SESSION['mongo']->$dbname;
	// TODO: handle errors

	// select a collection (analogous to a relational database's table)
	$_SESSION['collection'] = $_SESSION['db']->$collectionname;
}



/**
* my own print function
*
* @param mixed $os
*/
function o($os) {
	echo print_r($os, true) . "<br>";
}


/**
* callback function (reads xml and write to db
*
*/
function import_filme($update = true) {
	$each = safe_session(__FUNCTION__ . '_param_each');

	// xml error handler
	libxml_use_internal_errors(true);

	//	echo "cb: length(c)=" . strlen($c);
	//	echo "<pre>" . print_r(htmlentities($c), true) . "</pre>";
	$xml = simplexml_load_string(safe_session(__FUNCTION__ . '_param_xml'));

	// xml error handler
	if (!$xml) {
		$errors = libxml_get_errors();
		foreach ($errors as $error) {
			echo "<br>XML-Error: " . print_r($error, true);
			//echo "<pre>" . print_r(htmlentities($c), true) . "</pre>";
			die("DIE");

		}
		libxml_clear_errors();
	}

	$count = 0;
	foreach ($xml->$each as $film) {
		//		$x = $film->cast_crew;
		//		echo "id=" . $film->id . " Titel='" . $film->titel . "' . x='" . print_r($x, true) . "'<br>";
		//$document = array( 'id' => $film->id, 'titel' => (string)$film->titel, 'originalTitel' => (string)$film->originalTitel, 'cast_crew' => (array)$film->cast_crew );
		$document = (array)$film;
		$document['id'] = (int)$document['id'];
		$document['laufzeit'] = (int)$document['laufzeit'];
		$document['kinostart_ts'] = (int)strtotime($document['kinostart']);

		$document['threed'] = (boolean)$document['threed'];
		$document['reihe'] = (string)$document['reihe'];	// avoid to have empty objects ...
		$c = count($document['charts_deutschland']);
		if ($c > 0) {
			$document['charts_deutschland'] = (array)$document['charts_deutschland'];
			$document['charts_deutschland']['chart'] = (array)$document['charts_deutschland']['chart'];
			//TODO: hier auch schon weitere werte vorberechnen und ins document schreiben (z.B. $document['charts_deutschland']['besucher_we_gesamt'] und $document['charts_deutschland']['euro_gesamt'] kopien und besucher_gesamt aus dem letzten eintrag auch auf ebene davor... nicht in chart-array
			//TODO: in jqx framework einbauen!
			$document['charts_deutschland']['euro_gesamt'] = 0;
			$document['charts_deutschland']['besucher_we_gesamt'] = 0;
			$document['charts_deutschland']['besucher_gesamt'] = 0;
			for ($i = 0; $i < $c; $i++) {
				$document['charts_deutschland']['chart'][$i] = (array)$document['charts_deutschland']['chart'][$i];
				$document['charts_deutschland']['chart'][$i]['platz'] = (int)$document['charts_deutschland']['chart'][$i]['platz'];
				$document['charts_deutschland']['chart'][$i]['woche'] = (int)$document['charts_deutschland']['chart'][$i]['woche'];
				$document['charts_deutschland']['chart'][$i]['kopien'] = (int)$document['charts_deutschland']['chart'][$i]['kopien'];
				$document['charts_deutschland']['chart'][$i]['euro'] = (int)$document['charts_deutschland']['chart'][$i]['euro'];
				$document['charts_deutschland']['euro_gesamt'] += $document['charts_deutschland']['chart'][$i]['euro'];
				$document['charts_deutschland']['chart'][$i]['besucher_we'] = (int)$document['charts_deutschland']['chart'][$i]['besucher_we'];
				$document['charts_deutschland']['besucher_we_gesamt'] += $document['charts_deutschland']['chart'][$i]['besucher_we'];
				$document['charts_deutschland']['chart'][$i]['besucher_gesamt'] = (int)$document['charts_deutschland']['chart'][$i]['besucher_gesamt'];
				$document['charts_deutschland']['besucher_gesamt'] = $document['charts_deutschland']['chart'][$i]['besucher_gesamt'];
			}
		}
		//error_log("document=" . print_r($document, true));

		if ($update) {
			$query = array( id => $document['id'] );
			safe_session('collection')->update($query, $document, [ upsert => true, multi => false ]);
		} else {
			safe_session('collection')->insert($document);
		}
		//o($document);
		$count++;
	}

	return $count;
}


/**
* handle xml function to read xml in parts and call callback
*
* @param mixed $callback
* @param mixed $file
* @param mixed $root
* @param mixed $each
* @param mixed $partsize
*/
function handle_xml($callback, $file, $root, $each, $partsize = 1024000, $update = true) {
	echo 'file="' . $file . '"<br>';

	$root_s = '<' . $root . '>';
	$root_e = '</' . $root . '>';
	$each_s = '</' . $each . '>';

	$_SESSION[$callback . '_param_each'] = $each;

	// handle chunks
	$filelength = filesize($file);
	$offset = 0;
	$rest = '';

	$flag = true;
	$firstrun = true;
	$run = 1;
	$count = 0;

	do {
		$readsize = min($filelength - $offset, $partsize);
		$r = file_get_contents($file, NULL, NULL, $offset, $readsize);
		echo "run=$run<br>";
		ob_flush();flush();
		$run++;

		if ($firstrun) {
			$rp = strpos($r, $root_s, 0);
			$rp += ($rp !== false) ? strlen($root_s) : 0;
			$header = substr($r, 0, $rp);
			$firstrun = false;
		} else {
			$rp = 0;
		}

		$last_each = strrpos($r, $each_s);
		if ($last_each === false) {
			$rest .= $r;
			$offset += $partsize;
			continue;
		}
		$last_each += ($last_each !== false) ? strlen($each_s) : 0;


		//		$s = substr($r, $rp, $last_each - $rp);
		//		$part = $header . $rest . $s . $root_e;
		$_SESSION[$callback . '_param_xml'] = $header . $rest . substr($r, $rp, $last_each - $rp) . $root_e;

		$count += call_user_func($callback, $update);

		$rest = substr($r, $last_each);
		$offset += $partsize;
		if ($readsize < $partsize) {
			$flag = false;
		}
	} while ($flag);

	// release memory used
	$rest = '';
	$_SESSION[$callback . '_param_xml'] = '';

	echo "<br>found $count films";
}



/**
* main function
*
* @param mixed $partialUpdate
*/
function main($partialUpdate = true) {
	$root = 'filme';
	open_database('spyder', $root);

	if ($partialUpdate) {
		$file = 'movies_2013_12_19.xml';	// update (daily)
		$update = true;
	} else {
		$file = 'movies_2013_08_28_full.xml';	// full base
		$_SESSION['collection']->drop();
		$update = false;
	}
	$each = 'film';
	$partsize = 1024*1024*5;	// 5M parts to fetch from input file
	$callback = 'import_filme';

	// memory and time ...
	$before = memory_get_usage(true);
	$time_start = microtime(true);

	// read xml
	handle_xml($callback, $file, $root, $each, $partsize, $update);

	// memory and time ...
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	echo "<br>imported in $time seconds";
	$after = memory_get_usage(true);
	echo "<br>memory used=($after - $before)=" . ($after - $before);

	// print out db
	if (false) {
		o('all entries in db');
		// find everything in the collection
		$sort = array(_id => 1);	// order of entered in db
		$cursor = $_SESSION['collection']->find(array(), array(id => 1, titel => 1, laufzeit => 1))->sort($sort);
		// iterate through the results
		foreach ($cursor as $document) {
			o($document);
		}
	}
}




////////

write_html_header();

main(true);

write_html_footer();


/* filme mit mehr als 1 mio besuchern an irgendeinem wochenende
{
'charts_deutschland.chart' : { $elemMatch: {besucher_we : {$gte: 1000000}}}
}
*/

/* filme mit sandra bullock in diesem jahr
{
'cast_crew.person' : { $elemMatch: {voller_name: "Sandra Bullock"}}
// ,laufzeit : {$gt: 0}
,kinostart: {$gt: "2013-01-01"}
}
*/
?>
