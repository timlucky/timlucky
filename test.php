<?php

$pw = 'admin';
$pwmd5 = md5($pw);
echo "$pw = $pwmd5";
die();

$xmlstr = <<<XML
<?xml version='1.0' standalone='yes'?>
<movies name="test">
	<row>
		<field name="id">1</field>
		<field name="video_title">After Earth Official Trailer #1 (2013) - Will Smith Movie HD</field>
	</row>
	<row>
		<field name="id">3</field>
		<field name="video_title">blabla</field>
	</row>
	<row>
		<field name="id">14</field>
		<field name="video_title">letzter</field>
	</row>
 <movie>
  <title>PHP: Behind the Parser</title>
  <characters>
   <character>
    <name>Ms. Coder</name>
    <actor>Onlivia Actora</actor>
   </character>
   <character>
    <name>Mr. Coder</name>
    <actor>El Act&#211;r</actor>
   </character>
  </characters>
  <plot>
   So, this language. It's like, a programming language. Or is it a
   scripting language? All is revealed in this thrilling horror spoof
   of a documentary.
  </plot>
  <great-lines>
   <line>PHP solves all my web problems</line>
  </great-lines>
  <rating type="thumbs">7</rating>
  <rating type="stars">5</rating>
 </movie>
</movies>
XML;


//$movies = new SimpleXMLElement($xmlstr);
$movies = simplexml_load_string($xmlstr);

/* Access the <rating> nodes of the first movie.
* Output the rating scale, too. */
foreach ($movies->row as $row) {
	foreach ($row->field as $field) {
		$name = (string)$field['name'];
		$value = (string)$field;
		echo $name . ' - ' . $value . '<br>';;
	}
}

foreach ($movies->row[0]->field as $field) {
	$name = (string)$field['name'];
	$value = (string)$field;
	echo $name . ' - ' . $value . '<br>';;
}

foreach ($movies->movie[0]->rating as $rating) {
	switch((string) $rating['type']) { // Get attributes as element indices
		case 'thumbs':
			echo $rating, ' thumbs up';
			break;
		case 'stars':
			echo $rating, ' stars';
			break;
	}
}
?>
